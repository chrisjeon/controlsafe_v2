Controlsafe::Application.routes.draw do
  devise_for :users, controllers: { registrations: 'registrations' }
  devise_for :admin_users, ActiveAdmin::Devise.config
  ActiveAdmin.routes(self)
  # The priority is based upon order of creation: first created -> highest priority.
  # See how all your routes lay out with "rake routes".

  # You can have the root of your site routed with "root"
  root 'welcome#index'

  # WelcomeController
  get :terms_and_conditions, to: 'welcome#terms_and_conditions'
  get :swap_forms, to: 'welcome#swap_forms'
  get :send_email, to: 'welcome#send_email'
  
  # OrdersController
  get :order_due_diligence, to: 'orders#order_due_diligence'
  get :search_document_safes, to: 'orders#search_document_safes'
  get :add_to_cart, to: 'orders#add_to_cart'
  get :document_safe_orders, to: 'orders#document_safe_orders'
  get :review_due_diligence, to: 'orders#review_due_diligence'

  resources :document_safes do
    resources :documents
  end

  resources :documents

  resources :brokers do
    resources :orders
  end

  resources :lawyers do
    resources :orders
  end

  resources :orders do
    get :autocomplete_document_safe_street_address, on: :collection
  end
end
