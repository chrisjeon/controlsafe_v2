class CreateDocumentSafes < ActiveRecord::Migration
  def change
    create_table :document_safes do |t|
      t.string :building_name
      t.string :street_address
      t.string :city
      t.string :state
      t.string :zip
      t.string :custom_id
      t.belongs_to :manager
      t.timestamps
    end
  end
end
