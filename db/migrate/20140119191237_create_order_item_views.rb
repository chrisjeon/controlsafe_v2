class CreateOrderItemViews < ActiveRecord::Migration
  def change
    create_table :order_item_views do |t|
      t.belongs_to :order_item
      t.timestamps
    end
  end
end
