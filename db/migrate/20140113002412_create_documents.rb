class CreateDocuments < ActiveRecord::Migration
  def change
    create_table :documents do |t|
      t.string :type
      t.string :price
      t.date :publish_date
      t.attachment :doc
      t.belongs_to :document_safe
      t.timestamps
    end
  end
end
