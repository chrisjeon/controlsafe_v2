class CreateOrders < ActiveRecord::Migration
  def change
    create_table :orders do |t|
      t.string :price
      t.string :sales_tax
      t.string :total_price
      t.string :recipient_name
      t.string :recipient_email
      t.string :authorization_code
      t.string :apartment_number
      t.string :order_number
      t.belongs_to :customer_user
      t.belongs_to :document_safe

      t.timestamps
    end
  end
end