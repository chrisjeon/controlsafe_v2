class AddAttributesToUsers < ActiveRecord::Migration
  def change
    add_column :users, :type, :string
    add_column :users, :first_name, :string
    add_column :users, :last_name, :string
    add_column :users, :position, :string
    add_column :users, :phone_number, :string
    add_column :users, :company_name, :string
    add_column :users, :company_street_address, :string
    add_column :users, :company_state, :string
    add_column :users, :company_zip, :string
    add_column :users, :customer_uri, :string
    add_column :users, :credit_card_uri, :string
    add_column :users, :company_city, :string
    add_column :users, :bank_account_uri, :string
  end
end