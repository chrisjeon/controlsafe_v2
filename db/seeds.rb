manager = User.create(type: 'Manager',
                      email: 'manager@controlsafe.com',
                      password: 'password',
                      password: 'password',
                      position: ['Principal', 'Employee'].sample,
                      first_name: Faker::Name.first_name,
                      last_name: Faker::Name.last_name,
                      phone_number: '1-342-692-9425',
                      company_name: Faker::Company.name,
                      company_street_address: Faker::Address.street_address,
                      company_state: Faker::Address.state_abbr,
                      company_zip: Faker::Address.zip,
                      company_city: Faker::Address.city)
bank_account = Balanced::BankAccount.new(account_number: '9900000001',
                                         name: manager.fullname,
                                         routing_number: '121000358',
                                         type: 'checking').save
customer = Balanced::Customer.new(name: manager.fullname,
                                  email: manager.email)
                                  .add_bank_account(bank_account.attributes[:uri]).save
manager.bank_account_uri = bank_account.attributes[:uri]
manager.customer_uri = customer.attributes[:uri]
manager.save

5.times do
  manager.document_safes.create(building_name: Faker::Company.name,
                                street_address: Faker::Address.street_address,
                                city: Faker::Address.city,
                                state: Faker::Address.state,
                                zip: Faker::Address.zip,
                                custom_id: Faker::Code.isbn)
end

DocumentSafe.all.each do |document_safe|
  24.times do
    document_safe.documents.create(type: 'BoardMinute',
                                   price: '2.00',
                                   publish_date: Date.today - (1..100).to_a.sample,
                                   doc: File.new('db/doc_files/document.pdf'))
  end

  2.times do
    document_safe.documents.create(type: 'FinancialStatement',
                                   price: '75.00',
                                   publish_date: Date.today - (1..100).to_a.sample,
                                   doc: File.new('db/doc_files/document.pdf'))
  end

  document_safe.documents.create(type: 'OfferingPlan',
                                 price: '150.00',
                                 publish_date: Date.today - (1..100).to_a.sample,
                                 doc: File.new('db/doc_files/document.pdf'))
end

lawyer = User.create(type: 'Lawyer',
                     email: 'lawyer@controlsafe.com',
                     password: 'password',
                     password: 'password',
                     first_name: Faker::Name.first_name,
                     last_name: Faker::Name.last_name,
                     phone_number: '977.440.5060',
                     company_name: Faker::Company.name,
                     company_street_address: Faker::Address.street_address,
                     company_state: Faker::Address.state_abbr,
                     company_zip: Faker::Address.zip,
                     company_city: Faker::Address.city)

credit_card = Balanced::Card.new(card_number: '4916726667781756',
                                 expiration_year: '2016',
                                 expiration_month: '10',
                                 security_code: '111',
                                 name: lawyer.fullname,
                                 phone_number: '742-792-6652',
                                 city: lawyer.company_city,
                                 state: lawyer.company_state,
                                 postal_code: lawyer.company_zip,
                                 street_address: lawyer.company_street_address).save
customer = Balanced::Customer.new(name: lawyer.fullname,
                                  email: lawyer.email)
                                  .add_card(credit_card.attributes[:uri]).save
lawyer.credit_card_uri = credit_card.attributes[:uri]
lawyer.customer_uri = customer.attributes[:uri]
lawyer.save

broker = User.create(type: 'Broker',
                     email: 'broker@controlsafe.com',
                     password: 'password',
                     password: 'password',
                     first_name: Faker::Name.first_name,
                     last_name: Faker::Name.last_name,
                     phone_number: '742-792-1111',
                     company_name: Faker::Company.name,
                     company_street_address: Faker::Address.street_address,
                     company_state: Faker::Address.state_abbr,
                     company_zip: Faker::Address.zip,
                     company_city: Faker::Address.city)
credit_card = Balanced::Card.new(card_number: '4916726667781756',
                                 expiration_year: '2016',
                                 expiration_month: '10',
                                 security_code: '111',
                                 name: broker.fullname,
                                 phone_number: broker.phone_number,
                                 city: broker.company_city,
                                 state: broker.company_state,
                                 postal_code: broker.company_zip,
                                 street_address: broker.company_street_address).save
customer = Balanced::Customer.new(name: broker.fullname,
                                  email: broker.email)
                                  .add_card(credit_card.attributes[:uri]).save
broker.credit_card_uri = credit_card.attributes[:uri]
broker.customer_uri = customer.attributes[:uri]
broker.save

5.times do |i|
  lawyer.orders.create(price: '535.55',
                       sales_tax: '10.00',
                       total_price: '545.55',
                       recipient_name: Faker::Name.name,
                       recipient_email: Faker::Internet.email,
                       authorization_code: [SecureRandom.base64, SecureRandom.hex].sample.to_s,
                       apartment_number: '11',
                       order_number: "CSF-#{i}",
                       created_at: DateTime.now - (1..100).to_a.sample.days,
                       document_safe: DocumentSafe.all.sample)
end

lawyer.orders.each do |order|
  document_safe = order.document_safe
  order.order_items.create(document: document_safe.documents.where(type: 'OfferingPlan').first)
end

5.times do |i|
  broker.orders.create(price: '535.55',
                       sales_tax: '10.00',
                       total_price: '545.55',
                       recipient_name: Faker::Name.name,
                       recipient_email: Faker::Internet.email,
                       authorization_code: [SecureRandom.base64, SecureRandom.hex].sample.to_s,
                       apartment_number: '11',
                       order_number: "CSF-#{i}",
                       created_at: DateTime.now - (1..100).to_a.sample.days,
                       document_safe: DocumentSafe.all.sample)
end

broker.orders.each do |order|
  document_safe = order.document_safe
  order.order_items.create(document: document_safe.documents.where(type: 'OfferingPlan').first)
end