# Read about factories at https://github.com/thoughtbot/factory_girl

FactoryGirl.define do
  trait :basic_user do
    type 'basic_user'
    email Faker::Internet.email
    password 'password'
    password_confirmation 'password'
    first_name Faker::Name.first_name
    last_name Faker::Name.last_name
    phone_number '123-456-7890'
    company_name Faker::Company.name
    company_street_address Faker::Address.street_address
    company_state Faker::Address.state_abbr
    company_zip Faker::Address.zip
    company_city Faker::Address.city
  end

  factory :lawyer, traits: [:basic_user] do
    type 'Lawyer'
    customer_uri '/v1/customers/CU5c3D2TQ3Asw0KG8UoiZ5eG'
    credit_card_uri '/v1/marketplaces/TEST-MPJU8MDJZlw05cXJe4f168S/cards/CC5bbcADzWzhduSQI3FeGYv4'
  end

  factory :broker, traits: [:basic_user] do
    type 'Broker'
    customer_uri '/v1/customers/CU5mGulm24VyVUOfveJ60Jkz'
    credit_card_uri '/v1/marketplaces/TEST-MPJU8MDJZlw05cXJe4f168S/cards/CC5l9Zf6SHds7X3kDmgdEeyA'
  end

  factory :manager, traits: [:basic_user] do
    type 'Manager'
    position ['Principal', 'Employee'].sample
    customer_uri '/v1/customers/CU4Pc5NFhY94eYdUyhU6Utue'
    bank_account_uri '/v1/bank_accounts/BA4KLj767Kpuwqa2yJlCcbRU'
  end
end