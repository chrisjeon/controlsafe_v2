include ActionDispatch::TestProcess

FactoryGirl.define do
  trait :basic_document do
    type 'basic_document'
    price '1.00'
    publish_date Date.today - (1..100).to_a.sample.days
    doc do
      fixture_file_upload(Rails.root.join('db', 'doc_files', 'document.pdf'), 
        'application/pdf')
    end
    document_safe
  end

  factory :board_minute, traits: [:basic_document] do
    type 'BoardMinute'
    price '2.00'
  end

  factory :financial_statement, traits: [:basic_document] do
    type 'FinancialStatement'
    price '75.00'
  end

  factory :offering_plan, traits: [:basic_document] do
    type 'OfferingPlan'
    price '150.00'
    publish_date nil
  end
end
