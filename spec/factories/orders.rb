# Read about factories at https://github.com/thoughtbot/factory_girl

FactoryGirl.define do
  factory :order do
    price '535.55'
    sales_tax '10.00'
    total_price '545.55'
    recipient_name Faker::Name.name
    recipient_email Faker::Internet.email
    authorization_code [SecureRandom.base64, SecureRandom.hex].sample.to_s
    apartment_number '11'
    order_number 'CSF-11laldsf'
    customer_user_id 1
    document_safe
  end
end
