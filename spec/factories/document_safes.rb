# Read about factories at https://github.com/thoughtbot/factory_girl

FactoryGirl.define do
  factory :document_safe do
    building_name Faker::Company.name
    street_address Faker::Address.street_address
    city Faker::Address.city
    state Faker::Address.state
    zip Faker::Address.zip
    custom_id Faker::Code.isbn
    manager
  end
end
