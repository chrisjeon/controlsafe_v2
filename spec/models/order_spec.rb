require 'spec_helper'

describe Order do
  it { should have_many :order_items }
  it { should belong_to :customer_user }
  it { should belong_to :document_safe }
  it { should validate_presence_of :customer_user }
  it { should validate_presence_of :document_safe }
  it { should validate_presence_of :price }
  it { should validate_presence_of :sales_tax }
  it { should validate_presence_of :total_price }
  it { should validate_presence_of :recipient_name }
  it { should validate_presence_of :recipient_email }
  it { should validate_presence_of :apartment_number }
  it { should validate_uniqueness_of :authorization_code }

  let(:manager) { FactoryGirl.create(:manager) }
  let(:broker) { FactoryGirl.create(:broker, email: 'brokerfoobar@controlsafe.com') }
  let(:document_safe) { FactoryGirl.create(:document_safe) }

  describe 'authorization_code class method' do
    it 'should create a string' do
      Order.create_authorization_code.class.name.should == 'String'
    end
  end

  describe 'include_offering_plan' do
    describe 'with invalid documents' do
      before do
        2.times { FactoryGirl.create(:financial_statement, document_safe: document_safe) }
        24.times { FactoryGirl.create(:board_minute, document_safe: document_safe)}
        @order = FactoryGirl.create(:order, customer_user_id: broker.id, document_safe: document_safe)
      end

      it 'should return false' do
        @order.include_offering_plan? == false
      end
    end
    describe 'with valid documents' do
      before do
        FactoryGirl.create(:offering_plan, document_safe: document_safe)
        2.times do
          FactoryGirl.create(:financial_statement, document_safe: document_safe)
        end
        24.times do
          FactoryGirl.create(:board_minute, document_safe: document_safe)
        end
        @order = 
          FactoryGirl.create(:order, 
            customer_user_id: broker.id, document_safe: document_safe)
      end

      it 'should return true' do
        @order.include_offering_plan? == true
      end
    end
  end

  describe 'include_financial_statements' do
    describe 'with invalid documents' do
      before do
        24.times do
          FactoryGirl.create(:board_minute, document_safe: document_safe)
        end
        @order = 
          FactoryGirl.create(:order, 
            customer_user_id: broker.id, document_safe: document_safe)
      end

      it 'should return false' do
        @order.include_financial_statements? == false
      end
    end
    describe 'with valid documents' do
      before do
        2.times do
          FactoryGirl.create(:financial_statement, document_safe: document_safe)
        end
        24.times do
          FactoryGirl.create(:board_minute, document_safe: document_safe)
        end
        @order = 
          FactoryGirl.create(:order, 
            customer_user_id: broker.id, document_safe: document_safe)
      end

      it 'should return true' do
        @order.include_financial_statements? == true
      end
    end
  end

  describe 'include_board_minutes' do
    describe 'with invalid documents' do
      before do
        2.times { FactoryGirl.create(:financial_statement, document_safe: document_safe) }
        @order = FactoryGirl.create(:order, customer_user_id: broker.id, document_safe: document_safe)
      end

      it 'should return false' do
        @order.include_board_minutes? == false
      end
    end
    describe 'with valid documents' do
      before do
        2.times { FactoryGirl.create(:financial_statement, document_safe: document_safe) }
        24.times { FactoryGirl.create(:board_minute, document_safe: document_safe)}
        @order = FactoryGirl.create(:order, customer_user_id: broker.id, document_safe: document_safe)
      end

      it 'should return true' do
        @order.include_board_minutes? == true
      end
    end
  end

  describe 'financial_statements method' do
    before do
      2.times { FactoryGirl.create(:financial_statement, document_safe: document_safe) }
      24.times { FactoryGirl.create(:board_minute, document_safe: document_safe)}
      @order = FactoryGirl.create(:order, customer_user_id: broker.id, document_safe: document_safe)
    end

    it 'count should return 2' do
      @order.financial_statements.count == 2
    end
  end

  describe 'board_minutes method' do
    before do
      2.times { FactoryGirl.create(:financial_statement, document_safe: document_safe) }
      24.times { FactoryGirl.create(:board_minute, document_safe: document_safe)}
      @order = FactoryGirl.create(:order, customer_user_id: broker.id, document_safe: document_safe)
    end

    it 'count should return 24' do
      @order.board_minutes.count == 2
    end
  end
end