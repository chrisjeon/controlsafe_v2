require 'spec_helper'

describe Document do
  it { should have_many :order_items }
  it { should belong_to :document_safe }
  it { should validate_presence_of :type }
  it { should validate_presence_of :price }
  it { should validate_presence_of :doc }
  it { should validate_presence_of :document_safe }
end