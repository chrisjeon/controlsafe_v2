require 'spec_helper'

describe OrderItem do
  it { should have_many :order_item_views }
  it { should belong_to :order }
  it { should belong_to :document }
  it { should validate_presence_of :order }
  it { should validate_presence_of :document }
end