require 'spec_helper'

describe FinancialStatement do
  it { should validate_presence_of :publish_date }

  before do
    manager = FactoryGirl.create(:manager)
    document_safe = FactoryGirl.create(:document_safe, manager: manager)
    @financial_statement = 
      FactoryGirl.create(:financial_statement, document_safe: document_safe)
  end

  describe 'validation methods' do
    it 'offering_plan? should return false' do
      @financial_statement.offering_plan?.should be false
    end
    it 'board_minute? should return false' do
      @financial_statement.board_minute?.should be false
    end
    it 'financial_statement? should return true' do
      @financial_statement.financial_statement?.should be true
    end
  end
end
