require 'spec_helper'

describe OrderItemView do
  it { should belong_to :order_item }
  it { should validate_presence_of :order_item }
end
