require 'spec_helper'

describe User do
  it { should validate_presence_of :email }
  it { should validate_presence_of :type }
  it { should validate_presence_of :first_name }
  it { should validate_presence_of :last_name }
  it { should validate_presence_of :phone_number }
  it { should validate_presence_of :company_name }
  it { should validate_presence_of :company_street_address }
  it { should validate_presence_of :company_state }
  it { should validate_presence_of :company_zip }
  it { should validate_presence_of :company_city }
end