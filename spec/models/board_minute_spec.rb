require 'spec_helper'

describe BoardMinute do
  it { should validate_presence_of :publish_date }

  before do
    manager = FactoryGirl.create(:manager)
    document_safe = FactoryGirl.create(:document_safe, manager: manager)
    @board_minute = FactoryGirl.create(:board_minute, document_safe: document_safe)
  end

  describe 'validation methods' do
    it 'offering_plan? should return false' do
      @board_minute.offering_plan?.should be false
    end
    it 'board_minute? should return true' do
      @board_minute.board_minute?.should be true
    end
    it 'financial_statement? should return false' do
      @board_minute.financial_statement?.should be false
    end
  end
end