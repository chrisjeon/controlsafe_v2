require 'spec_helper'

describe DocumentSafe do
  it { should have_many :documents }
  it { should have_many :orders }
  it { should belong_to :manager }
  it { should validate_presence_of :building_name }
  it { should validate_presence_of :street_address }
  it { should validate_presence_of :city }
  it { should validate_presence_of :state }
  it { should validate_presence_of :zip }
  it { should validate_presence_of :manager }
end