require 'spec_helper'

describe Manager do
  it { should validate_presence_of :position }
  it { should have_many :document_safes }
end
