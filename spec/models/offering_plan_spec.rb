require 'spec_helper'

describe OfferingPlan do
  before do
    manager = FactoryGirl.create(:manager)
    document_safe = FactoryGirl.create(:document_safe, manager: manager)
    @offering_plan = 
      FactoryGirl.create(:offering_plan, document_safe: document_safe)
  end

  describe 'validation methods' do
    it 'offering_plan? should return true' do
      @offering_plan.offering_plan?.should be true
    end
    it 'board_minute? should return false' do
      @offering_plan.board_minute?.should be false
    end
    it 'financial_statement? should return false' do
      @offering_plan.financial_statement?.should be false
    end
  end
end