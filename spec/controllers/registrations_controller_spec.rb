require 'spec_helper'

describe RegistrationsController do
  include Devise::TestHelpers
  fixtures :all
  render_views

  before { @request.env['devise.mapping'] = Devise.mappings[:user] }

  describe '#create' do
    describe 'with invalid params' do
      describe 'as lawyer or broker' do
        it 'should not create a new user', :vcr, record: :new_episodes do
          expect { post :create,
            user: FactoryGirl.attributes_for(:lawyer, type: ''), 
            credit_card: { card_number: '4843338857801838', 
              expiration_year: '2020', expiration_month: '11', 
              security_code: '111', name: 'Foo Bar', 
              phone_number: '4102223333', city: 'New York', state: 'NY', 
              postal_code: '10036', street_address: '123 Maine St' } 
              }.to_not change(Lawyer, :count)
        end
      end
      describe 'as manager' do
        it 'should not create a new user', :vcr, record: :new_episodes do
          expect { post :create,
            user: FactoryGirl.attributes_for(:manager, 
              type: 'Manager', email: ''), 
            bank_account: { name: 'Foo Bar', routing_number: '121000358', 
              account_number: '9900000001' } }.to_not change(Manager, :count)
        end
      end
    end
    describe 'signing up as lawyer' do
      it 'should create a lawyer', :vcr, record: :new_episodes do
        expect { post :create, 
          user: FactoryGirl.attributes_for(:lawyer), 
          credit_card: { card_number: '4843338857801838', 
            expiration_year: '2020', expiration_month: '11', 
            security_code: '111', name: 'Foo Bar', phone_number: '4102223333', 
            city: 'New York', state: 'NY', postal_code: '10036', 
            street_address: '123 Maine St' } }.to change(Lawyer, :count).by(1)
      end
    end
    describe 'signing up as broker' do
      it 'should create a broker', :vcr, record: :new_episodes do
        expect { post :create, 
          user: FactoryGirl.attributes_for(:broker), 
          credit_card: { card_number: '4843338857801838', 
            expiration_year: '2020', expiration_month: '11', 
            security_code: '111', name: 'Foo Bar', phone_number: '4102223333', 
            city: 'New York', state: 'NY', postal_code: '10036', 
            street_address: '123 Maine St' } }.to change(Broker, :count).by(1)
      end
    end
    describe 'signing up as manager' do
      it 'should not create a manager', :vcr, record: :new_episodes do
        expect { post :create, 
          user: FactoryGirl.attributes_for(:manager), 
          bank_account: { name: 'Foo Bar', routing_number: '121000358', 
            account_number: '9900000001' } }.to change(User, :count).by(1)
      end
    end
  end

  describe '#update' do
    describe 'with valid params' do
      describe 'updating lawyer', :vcr, record: :new_episodes do
        before do
          post :create, user: FactoryGirl.attributes_for(:lawyer), 
            credit_card: { card_number: '4843338857801838', 
              expiration_year: '2020', expiration_month: '11', 
              security_code: '111', name: 'Foo Bar', phone_number: '4102223333', 
              city: 'New York', state: 'NY', postal_code: '10036', 
              street_address: '123 Maine St' }
          @lawyer = Lawyer.last
          sign_in @lawyer
          RegistrationsController.any_instance.stub(
            :account_update_params).and_return(
            { email: 'foo@bar.com', password: 'password', 
              password_confirmation: 'password', current_password: 'password', 
              first_name: Faker::Name.first_name, 
              last_name: Faker::Name.last_name, 
              phone_number: '123-456-7890', 
              company_name: Faker::Company.name, 
              company_street_address: Faker::Address.street_address, 
              company_state: Faker::Address.state_abbr, 
              company_zip: Faker::Address.zip, 
              company_city: Faker::Address.city, type: 'Lawyer' })
        end
        describe 'updating without credit card' do
          before do
            put :update, 
              user: { email: 'foo@bar.com', password: 'password', 
                password_confirmation: 'password', current_password: 'password', 
                first_name: Faker::Name.first_name, 
                last_name: Faker::Name.last_name, 
                phone_number: '123-456-7890', company_name: Faker::Company.name,
                company_street_address: Faker::Address.street_address, 
                company_state: Faker::Address.state_abbr, 
                company_zip: Faker::Address.zip, 
                company_city: Faker::Address.city, type: 'Lawyer' }, 
                credit_card: { 
                  card_number: '', expiration_year: '', expiration_month: '', 
                  security_code: '', name: '', phone_number: '', city: '', 
                  state: '', postal_code: '', street_address: '' }
          end
          it { should respond_with 302 }
        end
      end
      describe 'updating broker', :vcr, record: :new_episodes do
        before do
          post :create, 
            user: FactoryGirl.attributes_for(:broker), 
            credit_card: { card_number: '4843338857801838', 
              expiration_year: '2020', expiration_month: '11', 
              security_code: '111', name: 'Foo Bar', phone_number: '4102223333', 
              city: 'New York', state: 'NY', postal_code: '10036', 
              street_address: '123 Maine St' }
          @broker = Broker.last
          sign_in @broker
          RegistrationsController.any_instance.stub(
            :account_update_params).and_return(
            { email: 'foo@bar.com', password: 'password', 
              password_confirmation: 'password', current_password: 'password', 
              first_name: Faker::Name.first_name, 
              last_name: Faker::Name.last_name, phone_number: '123-456-7890', 
              company_name: Faker::Company.name, 
              company_street_address: Faker::Address.street_address, 
              company_state: Faker::Address.state_abbr, 
              company_zip: Faker::Address.zip, 
              company_city: Faker::Address.city, type: 'Broker' })
        end
        describe 'updating without credit card' do
          before do
            put :update, 
              user: { email: 'foo@bar.com', password: 'password', 
                password_confirmation: 'password', current_password: 'password', 
                first_name: Faker::Name.first_name, 
                last_name: Faker::Name.last_name, phone_number: '123-456-7890', 
                company_name: Faker::Company.name, 
                company_street_address: Faker::Address.street_address, 
                company_state: Faker::Address.state_abbr, 
                company_zip: Faker::Address.zip, 
                company_city: Faker::Address.city, type: 'Broker' }, 
                credit_card: { card_number: '', expiration_year: '', 
                  expiration_month: '', security_code: '', name: '', 
                  phone_number: '', city: '', state: '', 
                  postal_code: '', street_address: '' }
          end
          it { should respond_with 302 }
        end
      end
      describe 'updating manager', :vcr, record: :new_episodes do
        before do
          post :create, 
            user: FactoryGirl.attributes_for(:manager), 
            bank_account: { name: 'Foo Bar', routing_number: '121000358', 
              account_number: '9900000001' }
          @manager = Manager.last
          sign_in @manager
          RegistrationsController.any_instance.stub(
            :account_update_params).and_return(
            { email: 'foo@bar.com', password: 'password', 
              password_confirmation: 'password', current_password: 'password', 
              position: 'Principal', first_name: Faker::Name.first_name, 
              last_name: Faker::Name.last_name, phone_number: '123-456-7890', 
              company_name: Faker::Company.name, 
              company_street_address: Faker::Address.street_address, 
              company_state: Faker::Address.state_abbr, 
              company_zip: Faker::Address.zip, 
              company_city: Faker::Address.city, type: 'Manager' })
        end
        describe 'updating with bank account' do
          before do
            put :update, 
              user: { email: 'foo@bar.com', password: 'password', 
                password_confirmation: 'password', current_password: 'password', 
                first_name: Faker::Name.first_name, 
                last_name: Faker::Name.last_name, 
                phone_number: '123-456-7890', company_name: Faker::Company.name, 
                company_street_address: Faker::Address.street_address, 
                company_state: Faker::Address.state_abbr, 
                company_zip: Faker::Address.zip, 
                company_city: Faker::Address.city, type: 'Manager' }, 
                bank_account: { name: @manager.fullname, 
                  routing_number: '121000358', account_number: '9900000001' }
          end
          it { should respond_with 302 }
        end
        describe 'updating without bank account' do
          before do
            put :update, 
              user: { 
                email: 'foo@bar.com', password: 'password', 
                password_confirmation: 'password', current_password: 'password', 
                first_name: Faker::Name.first_name, 
                last_name: Faker::Name.last_name, 
                phone_number: '123-456-7890', company_name: Faker::Company.name, 
                company_street_address: Faker::Address.street_address, 
                company_state: Faker::Address.state_abbr, 
                company_zip: Faker::Address.zip, 
                company_city: Faker::Address.city, type: 'Manager' }, 
                bank_account: { 
                  name: '', routing_number: '', account_number: '' }
          end
          it { should respond_with 302 }
        end
      end
    end
  end
end
