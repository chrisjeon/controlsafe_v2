require 'spec_helper'

describe WelcomeController do
  describe '#terms_and_conditions' do
    describe 'manager' do
      before { get :terms_and_conditions, user_type: 'manager' }
      it { should respond_with :success }
    end
    describe 'lawyer' do
      before { get :terms_and_conditions, user_type: 'lawyer' }
      it { should respond_with :success }
    end
    describe 'broker' do
      before { get :terms_and_conditions, user_type: 'broker' }
      it { should respond_with :success }
    end
  end

  describe '#swap_forms' do
    describe 'when viewing manager forms' do
      before { xhr :get, :swap_forms, user_type: 'manager' }
      it { should respond_with :success }
    end
    describe 'when viewing lawyer or broker forms' do
      before { xhr :get, :swap_forms }
      it { should respond_with :success }
    end
  end

  describe '#send email' do
    before do
      get :send_email, name: 'Foo Bar', 
        email: 'foo@bar.com', subject: 'Foofoofoo', content: 'Barbarbar'
    end
    it { should respond_with :found }
  end
end