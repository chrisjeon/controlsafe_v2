require 'spec_helper'

describe OrdersController do
  let(:manager) { FactoryGirl.create(:manager, email: 'foo@bar.com') }
  let(:document_safe) { FactoryGirl.create(:document_safe, manager: manager) }

  describe 'when not signed in' do
    describe '#order_due_diligence' do
      before { get :order_due_diligence }
      it { should set_the_flash[:alert].to(
        'You need to sign in or sign up before continuing.') }
    end

    describe '#search_document_safe' do
      before { get :search_document_safes }
      it { should set_the_flash[:alert].to(
        'You need to sign in or sign up before continuing.') }
    end

    describe '#add_to_cart' do
      before { get :add_to_cart }
      it { should set_the_flash[:alert].to(
        'You need to sign in or sign up before continuing.') }
    end

    describe '#new' do
      before { get :new }
      it { should set_the_flash[:alert].to(
        'You need to sign in or sign up before continuing.') }
    end

    describe '#create' do
      before { post :create }
      it { should set_the_flash[:alert].to(
        'You need to sign in or sign up before continuing.') }
    end

    describe '#index' do
      before { get :index }
      it { should set_the_flash[:alert].to(
        'You need to sign in or sign up before continuing.') }
    end

    describe '#show' do
      before { get :show, id: 1 }
      it { should set_the_flash[:alert].to(
        'You need to sign in or sign up before continuing.') }
    end

    describe '#document_safe_orders' do
      before do
        get :document_safe_orders, document_safe_id: document_safe.id
      end
      it { should set_the_flash[:alert].to(
        'You need to sign in or sign up before continuing.') }
    end

    describe '#review_due_diligence' do
      before { get :review_due_diligence }
      it { should set_the_flash[:alert].to(
        'You need to sign in or sign up before continuing.') }
    end
  end

  describe 'when signed in as lawyer' do
    before do
      @lawyer = FactoryGirl.create(:lawyer)
      sign_in @lawyer
    end

    describe '#order_due_diligence' do
      describe 'before searching' do
        before { get :order_due_diligence }
        it { should respond_with :success }
      end

      describe 'when searching' do
        before { get :order_due_diligence, document_safe_id: document_safe.id }
        it { should respond_with :success }
      end
    end

    describe '#search_document_safe' do
      describe 'with invalid building_address' do
        before { get :search_document_safes, building_address: 'blahblahblah' }
        it { should set_the_flash[:alert].to('blahblahblah is not in our system.') }
      end
      describe 'with valid building_address' do
        before do
          get :search_document_safes, 
            building_address: document_safe.street_address
        end
        it { should_not set_the_flash[:alert].to(
          'blahblahblah is not in our system.') }
        it { should respond_with :redirect }
      end
    end

    describe '#add_to_cart' do
      describe 'invalid number of documents' do
        describe 'board minute test' do
          before do
            23.times do
              FactoryGirl.create(:board_minute, document_safe: document_safe)
            end
            get :add_to_cart, boardminute: BoardMinute.all.map(&:id),
                              document_safe_id: document_safe.id
          end
          it { should set_the_flash[:alert].to(
            'There was an error in your cart.') }
        end
        describe 'financial statement test' do
          before do
            FactoryGirl.create(:financial_statement, document_safe: document_safe)
            get :add_to_cart, financialstatement: FinancialStatement.all.map(&:id),
                              document_safe_id: document_safe.id
          end
          it { should set_the_flash[:alert].to(
            'There was an error in your cart.') }
        end
        describe 'with board minute and financial statement' do
          describe 'with valid financial statement and invalid board minute' do
            before do
              2.times do
                FactoryGirl.create(:financial_statement, document_safe: document_safe)
              end
              23.times do
                FactoryGirl.create(:board_minute, document_safe: document_safe)
              end
              get :add_to_cart, financialstatement: FinancialStatement.all.map(&:id),
                                boardminute: BoardMinute.all.map(&:id),
                                document_safe_id: document_safe.id
            end
            it { should set_the_flash[:alert].to(
              'There was an error in your cart.') }
          end
          describe 'with invalid financial statement and valid board minute' do
            before do
              FactoryGirl.create(:financial_statement, 
                document_safe: document_safe)
              24.times do
                FactoryGirl.create(:board_minute, document_safe: document_safe)
              end
              get :add_to_cart, financialstatement: FinancialStatement.all.map(&:id),
                                boardminute: BoardMinute.all.map(&:id),
                                document_safe_id: document_safe.id
            end
            it { should set_the_flash[:alert].to(
              'There was an error in your cart.') }
          end
        end
      end

      describe 'valid number of documents' do
        describe 'board_minute test' do
          before do
            24.times do
              FactoryGirl.create(:board_minute, document_safe: document_safe)
            end
            get :add_to_cart, boardminute: BoardMinute.all.map(&:id),
                              document_safe_id: document_safe.id
          end
          it { should_not set_the_flash[:alert].to(
            'There was an error in your cart.') }
        end
        describe 'financial statement test' do
          before do
            2.times do
              FactoryGirl.create(:financial_statement, document_safe: document_safe)
            end
            get :add_to_cart, financialstatement: FinancialStatement.all.map(&:id),
                              document_safe_id: document_safe.id
          end
          it { should_not set_the_flash[:alert].to(
            'There was an error in your cart.') }
        end
        describe 'when both board minute and financial statement are valid' do
          before do
            24.times do
              FactoryGirl.create(:board_minute, document_safe: document_safe)
            end
            2.times do
              FactoryGirl.create(:financial_statement, document_safe: document_safe)
            end
            get :add_to_cart, financialstatement: FinancialStatement.all.map(&:id),
                              boardminute: BoardMinute.all.map(&:id),
                              document_safe_id: document_safe.id
          end
          it { should_not set_the_flash[:alert].to(
            'There was an error in your cart.') }
        end
        describe 'when there are financial statements but not in cart' do
          before do
            FactoryGirl.create(:offering_plan, document_safe: document_safe)
            2.times do
              FactoryGirl.create(:financial_statement, document_safe: document_safe)
            end
            get :add_to_cart, offeringplan: OfferingPlan.all.map(&:id),
                              document_safe_id: document_safe.id
          end
          it { should_not set_the_flash[:alert].to(
            'There was an error in your cart.') }
        end
        describe 'when there are no financial statements in the system' do
          before do
            FactoryGirl.create(:offering_plan, document_safe: document_safe)
            get :add_to_cart, offeringplan: OfferingPlan.all.map(&:id),
                              document_safe_id: document_safe.id
          end
          it { should_not set_the_flash[:alert].to(
            'There was an error in your cart.') }
        end
        describe 'when there is only one financial statement in the system' do
          before do
            FactoryGirl.create(:offering_plan, document_safe: document_safe)
            FactoryGirl.create(:financial_statement, document_safe: document_safe)
            get :add_to_cart, offeringplan: OfferingPlan.all.map(&:id),
                              document_safe_id: document_safe.id
          end
          it { should_not set_the_flash[:alert].to(
            'There was an error in your cart.') }
        end
        describe 'when there are more than two financial statement in the cart with offering plan' do
          before do
            FactoryGirl.create(:offering_plan, document_safe: document_safe)
            4.times do
              FactoryGirl.create(:financial_statement, document_safe: document_safe)
            end
            get :add_to_cart, offeringplan: OfferingPlan.all.map(&:id),
                              financialstatement: FinancialStatement.all.map(&:id),
                              document_safe_id: document_safe.id
          end
          it { should_not set_the_flash[:alert].to(
            'There was an error in your cart.') }
        end
        describe 'when ordering missing documents' do
          describe 'when there are missing documents' do
            before do
              2.times do
                FactoryGirl.create(:financial_statement, document_safe: document_safe)
              end
              @order = 
                FactoryGirl.create(:order, 
                  customer_user_id: @lawyer.id, document_safe: document_safe)
              @order.order_items.create(document_id: FinancialStatement.first.id)
              get :add_to_cart, financialstatement: FinancialStatement.all.map(&:id).first,
                                document_safe_id: document_safe.id,
                                method_caller: 'order_missing_documents',
                                order_id: @order.id
            end
            it { should_not set_the_flash[:alert].to(
              'You have already have all of the documents for this building') }
          end

          describe 'when there are no missing documents' do
            before do
              2.times do
                FactoryGirl.create(:financial_statement, 
                  document_safe: document_safe)
              end
              @order = FactoryGirl.create(:order, 
                customer_user_id: @lawyer.id, document_safe: document_safe)
              FinancialStatement.all.each do |fs|
                @order.order_items.create(document_id: fs.id)
              end
              get :add_to_cart, document_safe_id: document_safe.id,
                                method_caller: 'order_missing_documents',
                                order_id: @order.id
            end
            it { should set_the_flash[:alert].to(
              'You already have all of the documents for this building') }
          end
        end
      end
    end
    describe '#new' do
      before do
        FactoryGirl.create(:offering_plan, document_safe: document_safe)
        2.times do
          FactoryGirl.create(:financial_statement, document_safe: document_safe)
        end
        24.times do
          FactoryGirl.create(:board_minute, document_safe: document_safe)
        end
        get :new, offeringplan: OfferingPlan.all.map(&:id),
                       freefinancialstatement: FinancialStatement.all.map(&:id),
                       boardminute: BoardMinute.all.map(&:id),
                       apartment_number: '11',
                       document_safe_id: document_safe.id
      end
      it { should respond_with :success }
    end

    describe '#create' do
      describe 'with invalid params' do
        before do
          FactoryGirl.create(:offering_plan, document_safe: document_safe)
          3.times do
            FactoryGirl.create(:financial_statement, document_safe: document_safe)
          end
          24.times do
            FactoryGirl.create(:board_minute, document_safe: document_safe)
          end
          post :create, 
            order: FactoryGirl.attributes_for(:order,
                                              customer_user: @lawyer,
                                              document_safe: document_safe,
                                              price: '')
        end
        it { should render_template 'new' }
      end
      describe 'with valid params', :vcr, record: :new_episodes do
        before do
          FactoryGirl.create(:offering_plan, document_safe: document_safe)
          3.times do
            FactoryGirl.create(:financial_statement, document_safe: document_safe)
          end
          24.times do
            FactoryGirl.create(:board_minute, document_safe: document_safe)
          end
          @create_method = -> {
            post :create, document_safe_id: document_safe.id, 
              order: FactoryGirl.attributes_for(:order, customer_user: @lawyer)
          }
        end
        it 'should create a new order' do
          expect { @create_method.call }.to change(Order, :count).by(1)
        end
      end
    end

    describe '#index' do
      before { get :index }
      it { should respond_with :success }
    end

    describe '#show' do
      before do
        @order = FactoryGirl.create(:order, 
          customer_user_id: @lawyer.id, document_safe: document_safe)
      end

      describe 'viewing own order' do
        before { xhr :get, :show, id: @order.id }
        it { should respond_with :success }
      end

      describe "viewing someone else's order" do
        before do
          @broker = FactoryGirl.create(:broker, email: 'broker@foobar.com')
          @second_order = FactoryGirl.create(:order, customer_user: @broker,
                                             document_safe: document_safe,
                                             authorization_code: '123')
          xhr :get, :show, id: @second_order.id
        end
        it { should respond_with :redirect }
        it { should set_the_flash[:alert].to('Access Denied') }
      end
    end

    describe '#document_safe_orders' do
      before do
        xhr :get, :document_safe_orders, document_safe_id: document_safe.id
      end
      it { should set_the_flash[:alert].to('Access Denied') }
    end

    describe '#review_due_diligence' do
      describe 'initial page view without search' do
        before { get :review_due_diligence }
        it { should respond_with :success }
      end

      describe 'when searching for authorization code' do
        describe 'with valid authorization code' do
          before do
            FactoryGirl.create(:offering_plan, document_safe: document_safe)
            2.times do
              FactoryGirl.create(:financial_statement, document_safe: document_safe)
            end
            24.times do
              FactoryGirl.create(:board_minute, document_safe: document_safe)
            end
            @order = FactoryGirl.create(:order, customer_user_id: @lawyer.id, document_safe: document_safe)
            get :review_due_diligence, authorization_code: @order.authorization_code
          end
          it { should respond_with :success }
        end

        describe 'with invalid authorization code' do
          before do
            FactoryGirl.create(:offering_plan, document_safe: document_safe)
            2.times do
              FactoryGirl.create(:financial_statement, document_safe: document_safe)
            end
            24.times do
              FactoryGirl.create(:board_minute, document_safe: document_safe)
            end
            @order = FactoryGirl.create(:order, customer_user_id: @lawyer.id, document_safe: document_safe)
            get :review_due_diligence, authorization_code: @order.authorization_code + '123'
          end
          it { should respond_with :redirect }
          it { should set_the_flash[:alert].to(
            'Authorization code is invalid.') }
        end
      end
    end
  end

  describe 'when signed in as broker' do
    before do
      @broker = FactoryGirl.create(:broker)
      sign_in @broker
    end

    describe 'before searching' do
      before { get :order_due_diligence }
      it { should respond_with :success }
    end

    describe 'when searching' do
      before { get :order_due_diligence, document_safe_id: document_safe.id }
      it { should respond_with :success }
    end
    describe '#add_to_cart' do
      describe 'invalid number of documents' do
        describe 'board minute test' do
          before do
            23.times do
              FactoryGirl.create(:board_minute, document_safe: document_safe)
            end
            get :add_to_cart, boardminute: BoardMinute.all.map(&:id),
                              document_safe_id: document_safe.id
          end
          it { should set_the_flash[:alert].to(
            'There was an error in your cart.') }
        end
        describe 'financial statement test' do
          before do
            FactoryGirl.create(:financial_statement, 
              document_safe: document_safe)
            get :add_to_cart, 
              financialstatement: FinancialStatement.all.map(&:id),
              document_safe_id: document_safe.id
          end
          it { should set_the_flash[:alert].to(
            'There was an error in your cart.') }
        end
        describe 'with board minute and financial statement' do
          describe 'with valid financial statement and invalid board minute' do
            before do
              2.times do
                FactoryGirl.create(:financial_statement, document_safe: document_safe)
              end
              23.times do
                FactoryGirl.create(:board_minute, document_safe: document_safe)
              end
              get :add_to_cart, 
                financialstatement: FinancialStatement.all.map(&:id),
                boardminute: BoardMinute.all.map(&:id),
                document_safe_id: document_safe.id
            end
            it { should set_the_flash[:alert].to(
              'There was an error in your cart.') }
          end
          describe 'with invalid financial statement and valid board minute' do
            before do
              FactoryGirl.create(:financial_statement, 
                document_safe: document_safe)
              24.times do
                FactoryGirl.create(:board_minute, document_safe: document_safe)
              end
              get :add_to_cart, 
                financialstatement: FinancialStatement.all.map(&:id),
                boardminute: BoardMinute.all.map(&:id),
                document_safe_id: document_safe.id
            end
            it { should set_the_flash[:alert].to(
              'There was an error in your cart.') }
          end
        end
      end

      describe 'valid number of documents' do
        describe 'board_minute test' do
          before do
            24.times do
              FactoryGirl.create(:board_minute, document_safe: document_safe)
            end
            get :add_to_cart, boardminute: BoardMinute.all.map(&:id),
                              document_safe_id: document_safe.id
          end
          it { should_not set_the_flash[:alert].to(
            'There was an error in your cart.') }
        end
        describe 'financial statement test' do
          before do
            2.times do
              FactoryGirl.create(:financial_statement, 
                document_safe: document_safe)
            end
            get :add_to_cart, 
              financialstatement: FinancialStatement.all.map(&:id),
              document_safe_id: document_safe.id
          end
          it { should_not set_the_flash[:alert].to(
            'There was an error in your cart.') }
        end
        describe 'when both board minute and financial statement are valid' do
          before do
            24.times do
              FactoryGirl.create(:board_minute, document_safe: document_safe)
            end
            2.times do
              FactoryGirl.create(:financial_statement, document_safe: document_safe)
            end
            get :add_to_cart, financialstatement: FinancialStatement.all.map(&:id),
                              boardminute: BoardMinute.all.map(&:id),
                              document_safe_id: document_safe.id
          end
          it { should_not set_the_flash[:alert].to(
            'There was an error in your cart.') }
        end
        describe 'when offering plan is valid' do
          before do
            FactoryGirl.create(:offering_plan, document_safe: document_safe)
            get :add_to_cart, offeringplan: OfferingPlan.all.map(&:id),
                              document_safe_id: document_safe.id
          end
          it { should_not set_the_flash[:alert].to(
            'There was an error in your cart.') }
        end
        describe 'when there are financial statements but not in cart' do
          before do
            FactoryGirl.create(:offering_plan, document_safe: document_safe)
            2.times do
              FactoryGirl.create(:financial_statement, 
                document_safe: document_safe)
            end
            get :add_to_cart, offeringplan: OfferingPlan.all.map(&:id),
                              document_safe_id: document_safe.id
          end
          it { should_not set_the_flash[:alert].to(
            'There was an error in your cart.') }
        end
        describe 'when there are no financial statements in the system' do
          before do
            FactoryGirl.create(:offering_plan, document_safe: document_safe)
            get :add_to_cart, offeringplan: OfferingPlan.all.map(&:id),
                              document_safe_id: document_safe.id
          end
          it { should_not set_the_flash[:alert].to(
            'There was an error in your cart.') }
        end
        describe 'when there is only one financial statement in the system' do
          before do
            FactoryGirl.create(:offering_plan, document_safe: document_safe)
            FactoryGirl.create(:financial_statement, 
              document_safe: document_safe)
            get :add_to_cart, offeringplan: OfferingPlan.all.map(&:id),
                              document_safe_id: document_safe.id
          end
          it { should_not set_the_flash[:alert].to(
            'There was an error in your cart.') }
        end
        describe 'when there are more than two financial statement in the cart with offering plan' do
          before do
            FactoryGirl.create(:offering_plan, document_safe: document_safe)
            4.times do
              FactoryGirl.create(:financial_statement, document_safe: document_safe)
            end
            get :add_to_cart, offeringplan: OfferingPlan.all.map(&:id),
                              financialstatement: FinancialStatement.all.map(&:id),
                              document_safe_id: document_safe.id
          end
          it { should_not set_the_flash[:alert].to(
            'There was an error in your cart.') }
        end
      end
    end
    describe '#new' do
      before do
        FactoryGirl.create(:offering_plan, document_safe: document_safe)
        2.times do
          FactoryGirl.create(:financial_statement, document_safe: document_safe)
        end
        24.times do
          FactoryGirl.create(:board_minute, document_safe: document_safe)
        end
        get :new, offeringplan: OfferingPlan.all.map(&:id),
                       freefinancialstatement: FinancialStatement.all.map(&:id),
                       financialstatement: [
                        FactoryGirl.create(
                          :financial_statement, document_safe: document_safe).id],
                       boardminute: BoardMinute.all.map(&:id),
                       apartment_number: '11',
                       document_safe_id: document_safe.id
      end
      it { should respond_with :success }
    end
    describe '#create' do
      describe 'with invalid params' do
        before do
          FactoryGirl.create(:offering_plan, document_safe: document_safe)
          3.times do
            FactoryGirl.create(:financial_statement, document_safe: document_safe)
          end
          24.times do
            FactoryGirl.create(:board_minute, document_safe: document_safe)
          end
          post :create, 
            order: FactoryGirl.attributes_for(:order,
                                              customer_user: @broker,
                                              document_safe: document_safe,
                                              price: '')
        end
        it { should render_template 'new' }
      end
      describe 'with valid params', :vcr, record: :new_episodes do
        before do
          FactoryGirl.create(:offering_plan, document_safe: document_safe)
          3.times do
            FactoryGirl.create(:financial_statement, document_safe: document_safe)
          end
          24.times do
            FactoryGirl.create(:board_minute, document_safe: document_safe)
          end
          @create_method = -> {
            post :create, document_safe_id: document_safe.id, 
              order: FactoryGirl.attributes_for(:order, customer_user: @broker)
          }
        end
        it 'should create a new order' do
          expect { @create_method.call }.to change(Order, :count).by(1)
        end
      end
    end
    describe '#index' do
      before { get :index }
      it { should respond_with :success }
    end

    describe '#show' do
      before do
        @order = FactoryGirl.create(:order, 
          customer_user: @broker, document_safe: document_safe)
      end

      describe 'viewing own order' do
        before { xhr :get, :show, id: @order.id }
        it { should respond_with :success }  
      end

      describe "viewing someone else's order" do
        before do
          @lawyer = FactoryGirl.create(:lawyer, email: 'lawyer@foobar.com')
          @second_order = FactoryGirl.create(:order,
                                             customer_user: @lawyer,
                                             document_safe: document_safe,
                                             authorization_code: '12345')
          xhr :get, :show, id: @second_order.id
        end
        it { should respond_with :redirect }
        it { should set_the_flash[:alert].to('Access Denied') }
      end
    end

    describe '#document_safe_orders' do
      before do
        xhr :get, :document_safe_orders, document_safe_id: document_safe.id
      end
      it { should set_the_flash[:alert].to('Access Denied') }
    end

    describe '#review_due_diligence' do
      before { get :review_due_diligence }
      it { should set_the_flash[:alert].to('Access Denied') }
    end
  end

  describe 'when signed in as manager' do
    before do
      @manager = FactoryGirl.create(:manager)
      sign_in @manager
    end

    describe '#order_due_diligence' do
      before { get :order_due_diligence }
      it { should set_the_flash[:alert].to('Access Denied') }
    end

    describe '#search_document_safe' do
      before { get :search_document_safes }
      it { should set_the_flash[:alert].to('Access Denied') }
    end

    describe '#add_to_cart' do
      before { get :add_to_cart }
      it { should set_the_flash[:alert].to('Access Denied') }
    end

    describe '#new' do
      before { get :new }
      it { should set_the_flash[:alert].to('Access Denied') }
    end

    describe '#create' do
      before { post :create }
      it { should set_the_flash[:alert].to('Access Denied') }
    end

    describe '#index' do
      before { get :index }
      it { should respond_with :success }
    end

    describe '#document_safe_orders' do
      before do
        xhr :get, :document_safe_orders, document_safe_id: document_safe.id
      end
      it { should respond_with :success }
    end

    describe '#show' do
      before do
        @broker = FactoryGirl.create(:broker, email: 'foobroker@goole.com')
        @order = FactoryGirl.create(:order, 
          customer_user: @broker, document_safe: document_safe)
        xhr :get, :show, id: @order.id
      end
      it { should respond_with :success }
    end

    describe '#review_due_diligence' do
      before { get :review_due_diligence }
      it { should set_the_flash[:alert].to('Access Denied') }
    end
  end
end