require 'spec_helper'

describe DocumentSafesController do
  describe 'when not signed in' do
    describe '#index' do
      before { get :index }
      it { should set_the_flash[:alert].to(
        'You need to sign in or sign up before continuing.') }
    end

    describe '#new' do
      before { get :new }
      it { should set_the_flash[:alert].to(
        'You need to sign in or sign up before continuing.') }
    end

    describe '#create' do
      it 'should not create a new document safe' do
        expect do
          post :create, document_safe: FactoryGirl.attributes_for(:document_safe)
        end.to_not change(DocumentSafe, :count)
      end
    end

    describe '#show' do
      before do
        @document_safe = FactoryGirl.create(
          :document_safe, manager: 
          FactoryGirl.create(:manager, email: 'foo@bar.com'))
        xhr :get, :show, id: @document_safe.id
      end
      it { should respond_with :unauthorized }
    end

    describe '#destroy' do
      before do
        document_safe = FactoryGirl.create(:document_safe, 
          manager: FactoryGirl.create(:manager, email: 'foo@bar.com'))
        @destroy_method = -> {
          delete :destroy, id: document_safe.id
        }
      end
      it 'should not destroy the document safe' do
        expect { @destroy_method.call }.to_not change(DocumentSafe, :count)
      end
    end

    describe '#update' do
      before do
        document_safe = FactoryGirl.create(:document_safe,
          manager: FactoryGirl.create(:manager, email: 'foo@bar.com'))
        put :update, id: document_safe.id, 
          document_safe: FactoryGirl.attributes_for(:document_safe)
      end
      it { should set_the_flash[:alert].to(
        'You need to sign in or sign up before continuing.') }
    end
  end

  describe 'when signed in as lawyer' do
    before do
      @lawyer = FactoryGirl.create(:lawyer)
      sign_in @lawyer
    end

    describe '#index' do
      before { get :index }
      it { should set_the_flash[:alert].to('Access Denied') }
    end

    describe '#new' do
      before { get :new }
      it { should set_the_flash[:alert].to('Access Denied') }
    end

    describe '#create' do
      it 'should not create a new document safe' do
        expect do
          post :create, document_safe: FactoryGirl.attributes_for(
            :document_safe, manager: @lawyer)
        end.to_not change(DocumentSafe, :count)
      end
    end

    describe '#show' do
      before do
        @document_safe = FactoryGirl.create(:document_safe, 
          manager: FactoryGirl.create(:manager, email: 'foo@bar.com'))
        xhr :get, :show, id: @document_safe.id
      end
      it { should set_the_flash[:alert].to('Access Denied') }
    end

    describe '#destroy' do
      before do
        document_safe = FactoryGirl.create(:document_safe, 
          manager: FactoryGirl.create(:manager, email: 'foo@bar.com'))
        @destroy_method = -> {
          delete :destroy, id: document_safe.id
        }
      end
      it 'should not destroy the document safe' do
        expect { @destroy_method.call }.to_not change(DocumentSafe, :count)
      end
    end

    describe '#update' do
      before do
        document_safe = FactoryGirl.create(:document_safe,
          manager: FactoryGirl.create(:manager, email: 'foo@bar.com'))
        put :update, id: document_safe.id, 
          document_safe: FactoryGirl.attributes_for(:document_safe)
      end
      it { should set_the_flash[:alert].to('Access Denied') }
    end
  end

  describe 'when signed in as broker' do
    before do
      @broker = FactoryGirl.create(:broker)
      sign_in @broker
    end

    describe '#index' do
      before { get :index }
      it { should set_the_flash[:alert].to('Access Denied') }
    end

    describe '#new' do
      before { get :new }
      it { should set_the_flash[:alert].to('Access Denied') }
    end

    describe '#create' do
      it 'should not create a new document safe' do
        expect do
          post :create, document_safe: FactoryGirl.attributes_for(
            :document_safe, manager: @broker)
        end.to_not change(DocumentSafe, :count)
      end
    end

    describe '#show' do
      before do
        @document_safe = FactoryGirl.create(:document_safe, 
          manager: FactoryGirl.create(:manager, email: 'foo@bar.com'))
        xhr :get, :show, id: @document_safe.id
      end
      it { should set_the_flash[:alert].to('Access Denied') }
    end

    describe '#destroy' do
      before do
        document_safe = FactoryGirl.create(:document_safe, 
          manager: FactoryGirl.create(:manager, email: 'foo@bar.com'))
        @destroy_method = -> {
          delete :destroy, id: document_safe.id
        }
      end
      it 'should not destroy the document safe' do
        expect { @destroy_method.call }.to_not change(DocumentSafe, :count)
      end
    end

    describe '#update' do
      before do
        document_safe = FactoryGirl.create(:document_safe, 
          manager: FactoryGirl.create(:manager, email: 'foo@bar.com'))
        put :update, id: document_safe.id, 
          document_safe: FactoryGirl.attributes_for(:document_safe)
      end
      it { should set_the_flash[:alert].to('Access Denied') }
    end
  end

  describe 'when signed in as manager' do
    before do
      @manager = FactoryGirl.create(:manager)
      sign_in @manager
    end

    describe '#index' do
      before { get :index }
      it { should respond_with :success }
    end

    describe '#new' do
      before { get :new }
      it { should respond_with :success }
    end

    describe '#create' do
      describe 'with valid params' do
        describe 'without option to create another document safe' do
          it 'should create a new document safe' do
            expect do
              post :create, document_safe: FactoryGirl.attributes_for(
                :document_safe, manager: @manager)
            end.to change(DocumentSafe, :count).by(1)
          end
        end
        describe 'with option to create another document safe' do
          before do
            post :create, commit: 'Create and Make Another Document Safe', 
              document_safe: FactoryGirl.attributes_for(
                :document_safe, manager: @manager)
          end
          it { should redirect_to new_document_safe_path }
        end
      end

      describe 'with invalid params' do
        it 'should not create a new document safe' do
          expect do
            post :create, document_safe: FactoryGirl.attributes_for(
              :document_safe, building_name: '', manager: @manager)
          end.to_not change(DocumentSafe, :count)
        end
      end
    end

    describe '#show' do
      before do
        @document_safe = FactoryGirl.create(:document_safe, manager: @manager)
        xhr :get, :show, id: @document_safe.id
      end
      it { should respond_with :success }
    end

    describe '#destroy' do
      before do
        document_safe = FactoryGirl.create(:document_safe, manager: @manager)
        @destroy_method = -> {
          delete :destroy, id: document_safe.id
        }
      end
      it 'should destroy the document safe' do
        expect { @destroy_method.call }.to change(DocumentSafe, :count).by(-1)
      end
    end

    describe '#update' do
      before do
        @document_safe = FactoryGirl.create(:document_safe, manager: @manager)
      end

      describe 'with invalid params' do
        before do
          put :update, id: @document_safe.id, 
            document_safe: { building_name: '' }
        end
        it { should set_the_flash[:alert].to('Document safe was not updated.') }
      end

      describe 'with valid params' do
        before do
          put :update, id: @document_safe.id, 
            document_safe: FactoryGirl.attributes_for(:document_safe)
        end
        it { should set_the_flash[:notice].to(
          'Document safe was successfully updated.') }
      end
    end
  end
end
