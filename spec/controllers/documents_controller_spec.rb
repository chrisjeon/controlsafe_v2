require 'spec_helper'

describe DocumentsController do
  let(:manager) { FactoryGirl.create(:manager, email: 'foo@bar.com') }
  let(:document_safe) { FactoryGirl.create(:document_safe, manager: manager) }

  describe 'when not signed in' do
    describe '#index' do
      before { get :index, document_safe_id: document_safe.id }
      it { should set_the_flash[:alert].to(
        'You need to sign in or sign up before continuing.') }
    end

    describe '#create' do
      before do
        post :create, document_safe_id: document_safe.id, 
          document: FactoryGirl.attributes_for(:board_minute)
      end
      it { should set_the_flash[:alert].to(
        'You need to sign in or sign up before continuing.') }
    end

    describe '#show' do
      before do
        get :show, id: FactoryGirl.create(:board_minute, 
          document_safe: document_safe)
      end
      it { should set_the_flash[:alert].to(
        'You need to sign in or sign up before continuing.') }
    end

    describe '#update' do
      before do
        @board_minute = FactoryGirl.create(:board_minute, 
          document_safe: document_safe)
        put :update, id: @board_minute.id, 
          document: FactoryGirl.attributes_for(:board_minute)
      end
      it { should set_the_flash[:alert].to(
        'You need to sign in or sign up before continuing.') }
    end

    describe '#destroy' do
      before do
        @board_minute = FactoryGirl.create(:board_minute, 
          document_safe: document_safe)
        delete :destroy, id: @board_minute.id
      end
      it { should set_the_flash[:alert].to(
        'You need to sign in or sign up before continuing.') }
    end

    describe '#edit' do
      before do
        @board_minute = FactoryGirl.create(:board_minute, 
          document_safe: document_safe)
        get :edit, id: @board_minute.id
      end
      it { should set_the_flash[:alert].to(
        'You need to sign in or sign up before continuing.') }
    end
  end

  describe 'when signed in as lawyer' do
    before do
      @lawyer = FactoryGirl.create(:lawyer)
      sign_in @lawyer
    end
    describe '#index' do
      before { get :index, document_safe_id: document_safe.id }
      it { should set_the_flash[:alert].to('Access Denied') }
    end
    describe '#create' do
      before do
        post :create, document_safe_id: document_safe.id, 
          document: FactoryGirl.attributes_for(:board_minute)
      end
      it { should set_the_flash[:alert].to('Access Denied') }
    end
    describe '#show' do
      describe 'when viewing without authorization code' do
        before do
          @board_minute = 
            FactoryGirl.create(:board_minute, document_safe: document_safe)
          @order = 
            FactoryGirl.create(:order, 
              customer_user_id: @lawyer.id, document_safe: document_safe)
          @order_item = 
            FactoryGirl.create(:order_item, 
              order: @order, document_id: @board_minute.id)
          xhr :get, :show, id: @board_minute.id
        end
        it { should respond_with :redirect }
        it { should set_the_flash[:alert].to('Access Denied') }
      end
      describe 'when viewing authorized documents' do
        before do
          @board_minute = 
            FactoryGirl.create(:board_minute, document_safe: document_safe)
          @order = 
            FactoryGirl.create(:order, 
              customer_user_id: @lawyer.id, 
              document_safe: document_safe, created_at: DateTime.now)
          @order_item = 
            FactoryGirl.create(:order_item, 
              order: @order, document_id: @board_minute.id)
          xhr :get, :show, id: @board_minute.id, order_id: @order.id
        end
        it { should respond_with :success }
      end
      describe 'when viewing unauthorized documents' do
        before do
          @board_minute = 
            FactoryGirl.create(:board_minute, document_safe: document_safe)
          @financial_statement = 
            FactoryGirl.create(:financial_statement, 
              document_safe: document_safe)
          @order = 
            FactoryGirl.create(:order, 
              customer_user_id: @lawyer.id, document_safe: document_safe)
          @order_item = 
            FactoryGirl.create(:order_item, 
              order: @order, document_id: @board_minute.id)
          xhr :get, :show, id: @financial_statement.id, order_id: @order.id
        end
        it { should respond_with :redirect }
        it { should set_the_flash[:alert].to('Access Denied') }
      end

      describe 'document viewing time window' do
        describe 'board minutes' do
          before do
            @board_minute = 
              FactoryGirl.create(:board_minute, document_safe: document_safe)
            @order = 
              FactoryGirl.create(:order, 
                customer_user_id: @lawyer.id, document_safe: document_safe)
            @order_item = 
              FactoryGirl.create(:order_item, 
                order: @order, document_id: @board_minute.id)
          end
          describe 'when viewing for the first time' do
            before do
              xhr :get, :show, id: @board_minute.id, order_id: @order.id
            end
            it { should respond_with :success }
            it { should_not set_the_flash[:alert].to('Board Minute has expired') }
          end
          describe 'when viewing for the first time' do
            before do
              xhr :get, :show, id: @board_minute.id, order_id: @order.id
              xhr :get, :show, id: @board_minute.id, order_id: @order.id
            end
            it { should respond_with :redirect }
            it { should set_the_flash[:alert].to('Board Minute has expired') }
          end
        end
        describe 'offering plan' do
          before do
            @offering_plan = 
              FactoryGirl.create(:offering_plan, document_safe: document_safe)
            @order = 
              FactoryGirl.create(:order, 
                customer_user_id: @lawyer.id, 
                document_safe: document_safe, created_at: DateTime.now)
            @order_item = 
              FactoryGirl.create(:order_item, 
                order: @order, document_id: @offering_plan.id)
          end
          describe 'when viewing within 30 day windows' do
            before do
              xhr :get, :show, id: @offering_plan.id, order_id: @order.id
            end
            it { should respond_with :success }
          end
          describe 'when viewing outside of 30 days window' do
            before do
              @order.created_at = DateTime.now - 32.days
              @order.save
              xhr :get, :show, id: @offering_plan.id, order_id: @order.id
            end
            it { should respond_with :redirect }
            it { should set_the_flash[:alert].to('Document has expired') }
          end
        end
      end
    end

    describe '#update' do
      before do
        @board_minute = 
          FactoryGirl.create(:board_minute, document_safe: document_safe)
        put :update, id: @board_minute.id, 
          document: FactoryGirl.attributes_for(:board_minute)
      end
      it { should set_the_flash[:alert].to('Access Denied') }
    end
    
    describe '#destroy' do
      before do
        @board_minute = 
          FactoryGirl.create(:board_minute, document_safe: document_safe)
        delete :destroy, id: @board_minute.id
      end
      it { should set_the_flash[:alert].to('Access Denied') }
    end

    describe '#edit' do
      before do
        @board_minute = 
          FactoryGirl.create(:board_minute, document_safe: document_safe)
        get :edit, id: @board_minute.id
      end
      it { should set_the_flash[:alert].to('Access Denied') }
    end
  end

  describe 'when signed in as broker' do
    before do
      @broker = FactoryGirl.create(:broker)
      sign_in @broker
    end
    describe '#index' do
      before { get :index, document_safe_id: document_safe.id }
      it { should set_the_flash[:alert].to('Access Denied') }
    end
    describe '#create' do
      before do
        post :create, document_safe_id: document_safe.id, 
          document: FactoryGirl.attributes_for(:board_minute)
      end
      it { should set_the_flash[:alert].to('Access Denied') }
    end
    describe '#show' do
      before do
        xhr :get, :show, 
          id: FactoryGirl.create(:board_minute, document_safe: document_safe)
      end
      it { should set_the_flash[:alert].to('Access Denied') }
    end
    describe '#update' do
      before do
        @board_minute = 
          FactoryGirl.create(:board_minute, document_safe: document_safe)
        put :update, id: @board_minute.id, 
          document: FactoryGirl.attributes_for(:board_minute)
      end
      it { should set_the_flash[:alert].to('Access Denied') }
    end
    
    describe '#destroy' do
      before do
        @board_minute = 
          FactoryGirl.create(:board_minute, document_safe: document_safe)
        delete :destroy, id: @board_minute.id
      end
      it { should set_the_flash[:alert].to('Access Denied') }
    end

    describe '#edit' do
      before do
        @board_minute = 
          FactoryGirl.create(:board_minute, document_safe: document_safe)
        get :edit, id: @board_minute.id
      end
      it { should set_the_flash[:alert].to('Access Denied') }
    end
  end

  describe 'when signed in as manager' do
    before do
      sign_in manager
    end

    describe '#index' do
      before { get :index, document_safe_id: document_safe.id }
      it { should respond_with :success }
    end

    describe '#create' do
      describe 'board minute' do
        describe 'with invalid params' do
          it 'should not create a new board minute' do
            expect do
              post :create, 
                document_safe_id: document_safe.id, 
                document: FactoryGirl.attributes_for(
                    :board_minute, publish_date: nil)
            end.to_not change(Document, :count)
          end
        end
        describe 'with valid params' do
          it 'should create a new board minute' do
            expect do
              post :create, document_safe_id: document_safe.id, 
                document: FactoryGirl.attributes_for(:board_minute)
            end.to change(Document, :count).by(1)
          end
        end
      end
      describe 'offering plan' do
        describe 'with invalid params' do
          it 'should not create a new offering plan' do
            expect do
              post :create, document_safe_id: document_safe.id, 
                document: FactoryGirl.attributes_for(:offering_plan, type: '')
            end.to_not change(Document, :count)
          end
        end
        describe 'with valid params' do
          it 'should create a new offering plan' do
            expect do
              post :create, document_safe_id: document_safe.id, 
                document: FactoryGirl.attributes_for(:board_minute)
            end.to change(Document, :count).by(1)
          end
        end
      end
      describe 'financial statement' do
        describe 'with invalid params' do
          it 'should not create a new financial statement' do
            expect do
              post :create, document_safe_id: document_safe.id, 
                document: FactoryGirl.attributes_for(:financial_statement, publish_date: nil)
            end.to_not change(Document, :count)
          end
        end
        describe 'with valid params' do
          it 'should create a new financial statement' do
            expect do
              post :create, document_safe_id: document_safe.id, 
                document: FactoryGirl.attributes_for(:financial_statement)
            end.to change(Document, :count).by(1)
          end
        end
      end
    end
    describe '#show' do
      describe 'board minute' do
        before do
          @board_minute = FactoryGirl.create(:board_minute, 
            document_safe: document_safe)
        end

        describe 'when viewing own document' do
          before { xhr :get, :show, id: @board_minute.id }
          it { should respond_with :found }
        end

        describe "when viewing someone else's document" do
          before do
            @new_manager =
              FactoryGirl.create(:manager, email: 'foo@example.com')
            @new_document_safe =
              FactoryGirl.create(:document_safe, manager: @new_manager)
            @new_board_minute =
              FactoryGirl.create(:board_minute, document_safe: @new_document_safe)
            xhr :get, :show, id: @new_board_minute.id
          end
          it { should set_the_flash[:alert].to('Access Denied') }
        end
      end
      describe 'offering plan' do
        before do
          @offering_plan = FactoryGirl.create(:offering_plan, 
            document_safe: document_safe)
        end

        describe 'when viewing own document' do
          before { xhr :get, :show, id: @offering_plan.id }
          it { should respond_with :found }
        end

        describe "when viewing someone else's document" do
          before do
            @new_manager = 
              FactoryGirl.create(:manager, email: 'foo@example.com')
            @new_document_safe = 
              FactoryGirl.create(:document_safe, manager: @new_manager)
            @offering_plan = 
              FactoryGirl.create(:offering_plan, document_safe: @new_document_safe)
            xhr :get, :show, id: @offering_plan.id
          end
          it { should set_the_flash[:alert].to('Access Denied') }
        end
      end
      describe 'financial statement' do
        before do
          @financial_statement = FactoryGirl.create(:financial_statement, 
            document_safe: document_safe)
        end

        describe 'when viewing own document' do
          before { xhr :get, :show, id: @financial_statement.id }
          it { should respond_with :found }
        end

        describe "when viewing someone else's document" do
          before do
            @new_manager =
              FactoryGirl.create(:manager, email: 'foo@example.com')
            @new_document_safe =
              FactoryGirl.create(:document_safe, manager: @new_manager)
            @financial_statement =
              FactoryGirl.create(:financial_statement, 
                document_safe: @new_document_safe)
            xhr :get, :show, id: @financial_statement.id
          end
          it { should set_the_flash[:alert].to('Access Denied') }
        end
      end
    end
    describe '#update' do
      before do
        @board_minute = FactoryGirl.create(:board_minute, 
          document_safe: document_safe)
      end
      describe 'when updating own document' do
        describe 'with valid attributes' do
          before do
            put :update, id: @board_minute.id, 
              document: FactoryGirl.attributes_for(:board_minute)
          end
          it { should_not set_the_flash[:alert].to('Access Denied') }
          it { should set_the_flash[:notice].to('Document successfully updated') }
        end

        describe 'with invalid attributes' do
          before do
            put :update, id: @board_minute.id, 
              document: FactoryGirl.attributes_for(:board_minute, price: '')
          end
          it { should_not set_the_flash[:alert].to('Access Denied') }
          it { should set_the_flash[:alert].to('Document update failed') }
        end
      end

      describe "when updating other's document" do
        before do
          @new_manager =
            FactoryGirl.create(:manager, email: 'managerfoooz@controlsafe.com')
          @document_safe =
            FactoryGirl.create(:document_safe, manager: @new_manager)
          @other_document =
            FactoryGirl.create(:financial_statement, document_safe: @document_safe)
          put :update, id: @other_document.id, 
            document: FactoryGirl.attributes_for(:board_minute)
        end
        it { should set_the_flash[:alert].to('Access Denied') }
      end
    end
      
    describe '#destroy' do
      before do
        @board_minute = FactoryGirl.create(:board_minute, 
          document_safe: document_safe)
      end

      describe 'when destroying own document' do
        it 'should delete a document' do
          expect do
            delete :destroy, id: @board_minute.id
          end.to change(Document, :count).by(-1)
        end
      end

      describe "when deleting someone else's document" do
        before do
          @new_manager =
            FactoryGirl.create(:manager, email: 'managerfoooz@controlsafe.com')
          @document_safe =
            FactoryGirl.create(:document_safe, manager: @new_manager)
          @other_document =
            FactoryGirl.create(:financial_statement, document_safe: @document_safe)
        end
        it 'should not delete a document' do
          expect do
            delete :destroy, id: @other_document.id
          end.to_not change(Document, :count)
        end
      end
    end

    describe '#edit' do
      before do
        @board_minute = 
          FactoryGirl.create(:board_minute, document_safe: document_safe)
      end

      describe 'editing own document' do
        before { get :edit, id: @board_minute.id }
        it { should respond_with :success }
      end

      describe "when editing someone else's document" do
        before do
          @new_manager =
            FactoryGirl.create(:manager, email: 'managerfoooz@controlsafe.com')
          @document_safe =
            FactoryGirl.create(:document_safe, manager: @new_manager)
          @other_document =
            FactoryGirl.create(:financial_statement, document_safe: @document_safe)
          get :edit, id: @other_document.id
        end
        it { should set_the_flash[:alert].to('Access Denied') }
      end
    end
  end
end