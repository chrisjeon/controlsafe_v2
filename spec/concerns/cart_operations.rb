require 'spec_helper'

describe CartOperations do
  
  let(:manager) { FactoryGirl.create(:manager) }
  let(:document_safe) { FactoryGirl.create(:document_safe) }

  describe 'CartValidator class' do
    describe 'optimize_cart method' do
      before do
        FactoryGirl.create(:offering_plan, document_safe: document_safe)
        5.times do
          FactoryGirl.create(:financial_statement, document_safe: document_safe)
        end
        @cart_validator = CartOperations::CartValidator.new(
                            { offeringplan: OfferingPlan.all.map(&:id),
                              financialstatement: FinancialStatement.all.map(&:id),
                              document_safe_id: document_safe.id })
      end

      it 'should return a specific hash' do
        financial_statements = FinancialStatement.all.map(&:id)
        free_financial_statements = []
        2.times { free_financial_statements << financial_statements.pop }
        @cart_validator.optimize_cart.should == { 
          offeringplan: OfferingPlan.all.map(&:id),
          freefinancialstatement: free_financial_statements.reverse,
          financialstatement: financial_statements,
          boardminute: [] }
      end
    end

    describe 'create_free_financial_statements' do
      describe 'there are no financial statements in the system' do
        before do
          FactoryGirl.create(:offering_plan, document_safe: document_safe)
          @cart_validator = CartOperations::CartValidator.new(
                              { offeringplan: OfferingPlan.all.map(&:id),
                                document_safe_id: document_safe.id })        
        end
        it 'should return blank for financial_statement array' do
          @cart_validator.instance_eval do
            create_free_financial_statements.blank?
          end.should == true
        end
      end

      describe 'there are one financial statements in the system' do
        before do
          FactoryGirl.create(:offering_plan, document_safe: document_safe)
          FactoryGirl.create(:financial_statement, document_safe: document_safe)
          @cart_validator = CartOperations::CartValidator.new(
                              { offeringplan: OfferingPlan.all.map(&:id),
                                document_safe_id: document_safe.id })
        end
        it 'should return blank for financial_statement array' do
          @cart_validator.instance_eval do
            create_free_financial_statements.count
          end.should == 1
        end
      end

      describe 'there are two or more financial statements in the system' do
        before do
          FactoryGirl.create(:offering_plan, document_safe: document_safe)
          3.times do
            FactoryGirl.create(:financial_statement, document_safe: document_safe)
          end
          @cart_validator = CartOperations::CartValidator.new(
                              { offeringplan: OfferingPlan.all.map(&:id),
                                document_safe_id: document_safe.id })        
        end
        it 'should return blank for financial_statement array' do
          @cart_validator.instance_eval do
            create_free_financial_statements.count
          end.should == 2
        end
      end
    end
  end

  describe 'CheckoutCalculator' do
    before do
      FactoryGirl.create(:offering_plan, 
        document_safe: document_safe, price: '200.00')
      5.times do
        FactoryGirl.create(:financial_statement, 
          document_safe: document_safe, price: '75.00')
      end
      @checkout_calculator = CartOperations::CheckoutCalculator.new(
                              { offeringplan: OfferingPlan.all.map(&:id),
                                financialstatement: FinancialStatement.all.map(&:id) })
    end

    it 'should return correct total price' do
      @checkout_calculator.cart_prices_info.should == { price: '575.00',
                                                        sales_tax: '51.03',
                                                        total_price: '626.03' }
    end
  end

  describe 'MissingDocumentFinder' do
    before do
      FactoryGirl.create(:offering_plan, document_safe: document_safe)
      2.times do
        FactoryGirl.create(:financial_statement, document_safe: document_safe)
      end
      24.times do
        FactoryGirl.create(:board_minute, document_safe: document_safe)
      end
      lawyer = FactoryGirl.create(:lawyer, email: 'lawyerfooz@bar.com')
      @order = FactoryGirl.create(:order, 
                customer_user_id: lawyer.id, document_safe: document_safe)
    end

    describe 'when ordering just offering plans' do
      before { @order.order_items.create(document: OfferingPlan.first) }

      it 'should return all financial plan and all board minutes' do
        missing_document_hash = 
          CartOperations::MissingDocumentFinder.new(@order.id).missing_document_hash
        missing_document_hash[:offeringplan].count == 0
        missing_document_hash[:financialstatement].count == 2
        missing_document_hash[:boardminute].count == 24
      end
    end

    describe 'when ordering offering plan and one financial statements' do
      before do
        @order.order_items.create(document: OfferingPlan.first)
        @order.order_items.create(document: FinancialStatement.first)
      end

      it 'should return one financial statement and all board minutes' do
        missing_document_hash = 
          CartOperations::MissingDocumentFinder.new(@order.id).missing_document_hash
        missing_document_hash[:offeringplan].count == 0
        missing_document_hash[:financialstatement].count == 1
        missing_document_hash[:boardminute].count == 24
      end
    end

    describe 'when ordering one financial_statement' do
      before do
        @order.order_items.create(document: FinancialStatement.first)
      end

      it 'should return one offering plan, one financial statement, and all board minutes' do
        missing_document_hash = 
          CartOperations::MissingDocumentFinder.new(@order.id).missing_document_hash
        missing_document_hash[:offeringplan].count == 1
        missing_document_hash[:financialstatement].count == 1
        missing_document_hash[:boardminute].count == 24
      end
    end

    describe 'when ordering three board minutes' do
      before do
        @order.order_items.create(document: BoardMinute.first)
        @order.order_items.create(document: BoardMinute.first)
        @order.order_items.create(document: BoardMinute.first)
      end

      it 'should return one offering plan, two financial statements, and 21 board minutes' do
        missing_document_hash = 
          CartOperations::MissingDocumentFinder.new(@order.id).missing_document_hash
        missing_document_hash[:offeringplan].count == 1
        missing_document_hash[:financialstatement].count == 2
        missing_document_hash[:boardminute].count == 21
      end
    end

    describe 'when ordering all board minutes' do
      before do
        BoardMinute.all.each do |board_minute|
          @order.order_items.create(document: board_minute)
        end
      end

      it 'should return one offering plan, two financial statements' do
        missing_document_hash = 
          CartOperations::MissingDocumentFinder.new(@order.id).missing_document_hash
        missing_document_hash[:offeringplan].count == 1
        missing_document_hash[:financialstatement].count == 2
        missing_document_hash[:boardminute].count == 0
      end
    end
  end
end