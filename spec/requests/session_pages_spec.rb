require 'spec_helper'

describe 'SessionPages' do
  subject { page }

  describe 'Registration' do
    before { visit root_path }

    it { should have_content 'About' }
    it { should_not have_content 'Sign out' }

    describe 'correct flash error messages', :vcr, record: :new_episodes do
      describe 'broker' do
        before do
          click_link 'Register'
          fill_in 'First name',                                       with: Faker::Name.first_name
          fill_in 'Last name',                                        with: Faker::Name.last_name
          fill_in 'lawyer-broker-registration-email',                 with: Faker::Internet.email
          fill_in 'lawyer-broker-registration-password',              with: 'password'
          fill_in 'lawyer-broker-registration-password-confirmation', with: 'password1'
          fill_in 'Phone number',                                     with: '(123) 456-0000'
          fill_in 'Company name',                                     with: Faker::Company.name
          fill_in 'Street address',                                   with: Faker::Address.street_address
          fill_in 'City',                                             with: Faker::Address.city
          select 'NY',                                                from: 'State'
          fill_in 'Zip',                                              with: Faker::Address.zip
          fill_in 'Credit Card Number',                               with: '4024007115217001'
          fill_in 'Expiration Year (must be 4 digits)',               with: '2033'
          fill_in 'Expiration Month',                                 with: '11'
          fill_in 'Security Code',                                    with: '111'
          fill_in 'Full Name',                                        with: 'Foo Ruby'
          fill_in 'Phone Number',                                     with: '(123) 456-0000'
          fill_in 'Billing Street Address',                           with: Faker::Address.street_address
          fill_in 'Billing City',                                     with: 'New York'
          select 'NY',                                                from: 'Billing State'
          fill_in 'Billing Zip',                                      with: Faker::Address.zip
          check 'terms_and_conditions_'
          click_button 'Register'
        end
        it { should have_content '1 error prohibited this broker from being saved:' }
      end

      describe 'lawyer', :vcr, record: :new_episodes do
        before do
          click_link 'Register'
          choose 'user_type_lawyer'
          fill_in 'First name',                                       with: Faker::Name.first_name
          fill_in 'Last name',                                        with: Faker::Name.last_name
          fill_in 'lawyer-broker-registration-email',                 with: Faker::Internet.email
          fill_in 'lawyer-broker-registration-password',              with: 'password'
          fill_in 'lawyer-broker-registration-password-confirmation', with: 'password1'
          fill_in 'Phone number',                                     with: '(123) 456-0000'
          fill_in 'Company name',                                     with: Faker::Company.name
          fill_in 'Street address',                                   with: Faker::Address.street_address
          fill_in 'City',                                             with: Faker::Address.city
          select 'NY',                                                from: 'State'
          fill_in 'Zip',                                              with: Faker::Address.zip
          fill_in 'Credit Card Number',                               with: '4024007115217001'
          fill_in 'Expiration Year (must be 4 digits)',               with: '2033'
          fill_in 'Expiration Month',                                 with: '11'
          fill_in 'Security Code',                                    with: '111'
          fill_in 'Full Name',                                        with: 'Foo Ruby'
          fill_in 'Phone Number',                                     with: '(123) 456-0000'
          fill_in 'Billing Street Address',                           with: Faker::Address.street_address
          fill_in 'Billing City',                                     with: 'New York'
          select 'NY',                                                from: 'Billing State'
          fill_in 'Billing Zip',                                      with: Faker::Address.zip
          check 'terms_and_conditions_'
          click_button 'Register'
        end
        it { should have_content '1 error prohibited this lawyer from being saved:' }
      end

      pending 'manager invalid register' do
        describe 'manager', :vcr, record: :new_episodes, js: true do
          before do
            click_link 'Register'
            choose 'user_type_manager'
            fill_in 'First name',                                       with: Faker::Name.first_name
            fill_in 'Last name',                                        with: Faker::Name.last_name
            fill_in 'manager-registration-email',                       with: Faker::Internet.email
            fill_in 'manager-registration-password',                    with: 'password'
            fill_in 'manager-registration-password-confirmation',       with: 'password1'
            fill_in 'Phone number',                                     with: '(123) 456-0000'
            fill_in 'Company name',                                     with: Faker::Company.name
            fill_in 'Street address',                                   with: Faker::Address.street_address
            fill_in 'City',                                             with: Faker::Address.city
            select 'NY',                                                from: 'State'
            fill_in 'Zip',                                              with: Faker::Address.zip
            fill_in 'bank_account_name',                                with: Faker::Company.name
            fill_in 'bank_account_routing_number',                      with: '121000358'
            fill_in 'bank_account_account_number',                      with: '9900000001'
            check 'terms_and_conditions_'
            click_button 'Register'
          end
          it { should have_content '1 error prohibited this broker from being saved:' }
        end
      end
    end
  end
end