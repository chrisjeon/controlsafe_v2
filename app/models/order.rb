class Order < ActiveRecord::Base
  has_many :order_items
  belongs_to :customer_user
  belongs_to :document_safe
  validates :customer_user, presence: true
  validates :document_safe, presence: true
  validates :price, presence: true
  validates :sales_tax, presence: true
  validates :total_price, presence: true
  validates :recipient_name, presence: true
  validates :recipient_email, presence: true, 
            format: { with: /\A[\w+\-.]+@[a-z\d\-]+(\.[a-z]+)*\.[a-z]+\z/i }
  validates :apartment_number, presence: true
  validates :authorization_code, uniqueness: true

  def self.create_authorization_code
    o = [('a'..'z'), ('A'..'Z')].map { |i| i.to_a }.flatten
    string = (0...50).map { o[rand(o.length)] }.join
    authorization_code = [SecureRandom.base64, SecureRandom.hex, string].sample.to_s
  end

  def create_order_number
    update_attribute(:order_number, "CSF-%.6d" % id)
  end

  def include_offering_plan?
    order_items.map(&:document).map(&:type).include? 'OfferingPlan'
  end

  def include_financial_statements?
    order_items.map(&:document).map(&:type).include? 'FinancialStatement'
  end

  def include_board_minutes?
    order_items.map(&:document).map(&:type).include? 'BoardMinute'
  end

  def offering_plan
    order_items.map(&:document).map do |document|
      document if document.offering_plan?
    end.reject do |document| 
      document.nil?
    end.sort { |a, b| b.publish_date <=> a.publish_date }
  end

  def financial_statements
    order_items.map(&:document).map do |document|
      document if document.financial_statement?
    end.reject do |document|
      document.nil?
    end.sort { |a, b| b.publish_date <=> a.publish_date }
  end

  def board_minutes
    order_items.map(&:document).map do |document|
      document if document.board_minute?
    end.reject do |document|
      document.nil?
    end.sort { |a, b| b.publish_date <=> a.publish_date }
  end

  def expired?
    (Date.today - created_at.to_date).to_i > 30
  end
end