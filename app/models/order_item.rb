class OrderItem < ActiveRecord::Base
  has_many :order_item_views
  belongs_to :order
  belongs_to :document
  validates :order, presence: true
  validates :document, presence: true
end
