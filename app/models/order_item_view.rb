class OrderItemView < ActiveRecord::Base
  belongs_to :order_item
  validates :order_item, presence: true
end