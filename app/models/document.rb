class Document < ActiveRecord::Base
  has_many :order_items
  belongs_to :document_safe
  has_attached_file :doc,
                    s3_protocol: 'https',
                    s3_credentials: S3_CREDENTIALS,
                    s3_permissions: :private,
                    path: 'documents/:id/:filename'
  validates_attachment :doc, presence: true, 
                        content_type: { content_type: "application/pdf" }
  validates :type, presence: true
  validates :price, presence: true, format: { with: /\d+[.]\d{2}/ }
  validates :document_safe, presence: true

  def offering_plan?
    type == 'OfferingPlan'
  end

  def board_minute?
    type == 'BoardMinute'
  end

  def financial_statement?
    type == 'FinancialStatement'
  end
end