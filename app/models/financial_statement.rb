class FinancialStatement < Document
  validates :publish_date, presence: true
  validate :minimum_price

  def minimum_price
    errors.add(:price, "can't be less than $75.00") if self.price.to_f < 75.00
  end
end
