class Manager < User
  has_many :document_safes
  validates :position, presence: true
end
