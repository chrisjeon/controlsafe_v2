class Ability
  include CanCan::Ability

  def initialize(user)
    user ||= User.new
    if user.manager?
      can [:index, :new, :create], DocumentSafe
      can [:update, :destroy, :show], DocumentSafe do |document_safe|
        document_safe.try(:manager) == user
      end

      can :create, Document
      can [:show, :index, :update, :destroy, :edit], Document do |document|
        document.document_safe.try(:manager) == user
      end
      can [:index, :document_safe_orders, :show], Order
    elsif user.lawyer?
      can :show, Document
      can [:order_due_diligence,
           :autocomplete_document_safe_street_address,
           :search_document_safes,
           :add_to_cart,
           :new,
           :preload_checkout_documents,
           :create,
           :index,
           :review_due_diligence], Order
      can :show, Order do |order|
        order.try(:customer_user) == user
      end
    elsif user.broker?
      can [:order_due_diligence,
           :autocomplete_document_safe_street_address,
           :search_document_safes,
           :add_to_cart,
           :new,
           :preload_checkout_documents,
           :create,
           :index], Order
      can :show, Order do |order|
        order.try(:customer_user) == user
      end
    end
  end
end