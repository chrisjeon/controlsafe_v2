class BoardMinute < Document
  validates :publish_date, presence: true
  validate :minimum_price

  def minimum_price
    errors.add(:price, "can't be less than $2.00") if self.price.to_f < 2.00
  end
end
