class User < ActiveRecord::Base
  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :trackable, :validatable
  validates :email, presence: true 
  validates :type, presence: true 
  validates :first_name, presence: true 
  validates :last_name, presence: true 
  validates :phone_number, presence: true 
  validates :company_name, presence: true 
  validates :company_street_address, presence: true 
  validates :company_state, presence: true 
  validates :company_zip, presence: true 
  validates :company_city, presence: true 

  def fullname
    "#{first_name} #{last_name}"
  end

  def manager?
    type == 'Manager'
  end

  def lawyer?
    type == 'Lawyer'
  end

  def broker?
    type == 'Broker'
  end
end