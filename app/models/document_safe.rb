class DocumentSafe < ActiveRecord::Base
  has_many :documents
  has_many :financial_statements
  has_many :offering_plans
  has_many :board_minutes
  has_many :orders
  belongs_to :manager
  validates :building_name, presence: true
  validates :street_address, presence: true
  validates :city, presence: true
  validates :state, presence: true
  validates :zip, presence: true
  validates :manager, presence: true
end