class OfferingPlan < Document
  validate :minimum_price

  def minimum_price
    errors.add(:price, "can't be less than $150.00") if self.price.to_f < 150.00
  end
end
