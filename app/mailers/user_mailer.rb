class UserMailer < ActionMailer::Base
  default from: 'info@controlsafe.com'

  def order_confirmation_buyer_email(buyer_email, order_number)
    mail to: buyer_email, subject: "RE: Order Confirmation: #{order_number}"
  end

  def order_confirmation_lawyer_email(lawyer_email, authorization_code)
    @authorization_code = authorization_code
    @url = "https://www.controlsafe.com"
    mail to: lawyer_email, subject: 'RE: Due Diligence Documents'
  end

  def contact_form_email(name, email, subject, content)
    @name = name
    @content = content
    @email = email
    mail to: 'info@controlsafe.com', subject: subject
  end
end
