highlightSelected = (clickedLink) ->
  $('.custom-active').attr('id', '')
  clickedLink.attr('id', 'active')

$ ->
  $('.order-number-link a').on 'click', () ->  
    highlightSelected($(this))
    $.get $(this).attr('href'), {}, (data) ->
      $('.order-show').html(data['html'])

  $('.document-safe-orders-title a').on 'click', () ->
    $('.list-group-item.document-safe-orders-title').attr('class', 'list-group-item document-safe-orders-title')
    $(this).closest('li').attr('class', 'list-group-item document-safe-orders-title active')
    $.get $(this).attr('href'), {}, (data) ->
      console.log data['html']
      $('.document-safe-orders').html(data['html'])
      $('.order-number-link a').on 'click', () ->
        $.get $(this).attr('href'), {}, (data) ->
          console.log data['html']
          $('.document-safe-orders-information').html(data['html'])