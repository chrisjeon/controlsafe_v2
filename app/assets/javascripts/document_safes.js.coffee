$ ->
  $('.document-safe-title a').on 'click', () ->  
    $('.list-group-item.document-safe-title').attr('class', 'list-group-item document-safe-title')
    $(this).closest('li').attr('class', 'list-group-item document-safe-title active')
    $.get $(this).attr('href'), {}, (data) ->
      $('.document-safe-show').html(data['html'])