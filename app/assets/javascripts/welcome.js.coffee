setTermsAndConditions = (userType) ->
  if userType is 'user_type_manager'
    $('.terms-and-conditions-link').attr('href', '/terms_and_conditions?user_type=manager')
    $('.warning-message').html("Warning! It is a crime (NYS Real Property Law Art. 12A Sec. 442-e) for an unlicensed person to act as the licensed real estate broker of a managing agent.")
  else if userType is 'user_type_broker'
    $('.terms-and-conditions-link').attr('href', '/terms_and_conditions?user_type=broker')
    $('.warning-message').html("Warning! It is a crime (NYS Real Property Law Art. 12A Sec. 442-e) for an unlicensed person to act as a licensed real estate broker or licensed salesperson.")
  else if userType is 'user_type_lawyer'
    $('.terms-and-conditions-link').attr('href', '/terms_and_conditions?user_type=lawyer')
    $('.warning-message').html("Warning! It is a class E Felony (NYS Judiciary Law Sec. 486) to impersonate an attorney.")

swapRegistrationForm = (userType) ->
  if userType is 'user_type_manager'
    $.get '/swap_forms', { user_type: 'manager' }, (template) ->
      $('.user-information-registration').html(template['html'])
  else if userType is 'user_type_broker'
    $.get '/swap_forms', { user_type: 'broker' }, (template) ->
      $('.user-information-registration').html(template['html'])
  else if userType is 'user_type_lawyer'
    $.get '/swap_forms', { user_type: 'lawyer' }, (template) ->
      $('.user-information-registration').html(template['html'])
  setTermsAndConditions(userType)

$ ->
  selectedUserType = $("input[type='radio']:checked").attr('id')
  setTermsAndConditions(selectedUserType)

  $('.btn.btn-default.user-type').on 'click', () ->
    swapRegistrationForm($(this).attr('id'))

  $('.support-questions').click (event) ->
    event.preventDefault()
    $(this).parent().find('.support-answers').slideToggle()