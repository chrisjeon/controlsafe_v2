choose_price_options = (document_type) ->
  if document_type is 'BoardMinute'
    "<option value='2.00'>$2.00</option>
     <option value='3.00'>$3.00</option>
     <option value='4.00'>$4.00</option>"
  else if document_type is 'FinancialStatement'
    "<option value='75.00'>$75.00</option>
     <option value='80.00'>$80.00</option>
     <option value='85.00'>$85.00</option>
     <option value='90.00'>$90.00</option>
     <option value='95.00'>$95.00</option>
     <option value='100.00'>$100.00</option>
     <option value='105.00'>$105.00</option>
     <option value='110.00'>$110.00</option>
     <option value='115.00'>$115.00</option>
     <option value='120.00'>$120.00</option>
     <option value='125.00'>$125.00</option>
     <option value='130.00'>$130.00</option>
     <option value='135.00'>$135.00</option>
     <option value='140.00'>$140.00</option>
     <option value='145.00'>$145.00</option>
     <option value='150.00'>$150.00</option>"
  else if document_type is 'OfferingPlan'
    "<option value='150.00'>$150.00</option>
     <option value='160.00'>$160.00</option>
     <option value='170.00'>$170.00</option>
     <option value='180.00'>$180.00</option>
     <option value='190.00'>$190.00</option>
     <option value='200.00'>$200.00</option>
     <option value='210.00'>$210.00</option>
     <option value='220.00'>$220.00</option>
     <option value='230.00'>$230.00</option>
     <option value='240.00'>$240.00</option>
     <option value='250.00'>$250.00</option>
     <option value='260.00'>$260.00</option>
     <option value='270.00'>$270.00</option>
     <option value='280.00'>$280.00</option>
     <option value='290.00'>$290.00</option>
     <option value='300.00'>$300.00</option>"
$ ->
  $('#document_type').on 'change', ->
    document_type = $(this).find(':selected').text()
    if document_type is 'OfferingPlan'
      $('.publish-date-select').hide()
    else
      $('.publish-date-select').show()
    price_options = choose_price_options(document_type)
    $('#document_price').html(price_options)

  $('.document-links').on 'click', () ->
    $('.list-group-item.document-show-title-link').attr('class', 'list-group-item document-show-title-link')
    $(this).closest('li').attr('class', 'list-group-item document-show-title-link active')
    $.get $(this).attr('href'), {}, (data) ->
      $('.document-show-section').html(data['html'])

  $('.datepicker').datepicker(format: 'yyyy-mm-dd')