class DocumentsController < ApplicationController
  before_action :authenticate_user!
  load_and_authorize_resource
  before_action :clean_document_price, only: [:create, :update]

  def index
    session[:last_document_safe_page] = request.env['HTTP_REFERER']
    @document_safe = DocumentSafe.find(params[:document_safe_id])
    @documents = @document_safe.documents.order(publish_date: :desc).paginate(
      page: params[:page], per_page: 30)
  end

  def create
    @document_safe = DocumentSafe.find(params[:document_safe_id])
    @document = @document_safe.documents.new(document_params)
    @document.publish_date = nil if @document.type == 'OfferingPlan'
    if @document.save
      redirect_to document_safe_documents_path(@document_safe), 
        notice: 'Document was successfully added'
    else
      redirect_to document_safe_documents_path(@document_safe), 
        alert: 'Document was not successfully added'
    end
  end

  def edit
  end

  def update
    @document_safe = @document.document_safe
    if @document.update_attributes(document_params)
      redirect_to documents_path(document_safe_id: @document_safe.id), 
        notice: 'Document successfully updated'
    else
      redirect_to documents_path(document_safe_id: @document_safe.id), 
        alert: 'Document update failed'
    end
  end

  def destroy
    @document_safe = @document.document_safe
    @document.destroy
    redirect_to document_safe_documents_path(@document_safe), 
      notice: 'Document was successfully destroyed'
  end

  def show
    respond_to do |format|
      format.json do
        render json: { 
          html: render_to_string(
            template: 'documents/show.html.haml', layout: false) }
      end
      format.html { order_item_check }
    end
  end

  private

  def document_params
    params.require(:document).permit(:type, :price, :publish_date, :doc)
  end

  def order_item_check
    if params[:order_id].blank?
      redirect_to root_path, alert: 'Access Denied'
    else
      @order = Order.find(params[:order_id])
      if order_has_document?
        @order_item = @order.order_items.find_by(document_id: @document.id)
        @order_item.order_item_views.create
        if @document.board_minute?
          if @order_item.order_item_views.count > 1
            redirect_to root_path, alert: 'Board Minute has expired'
          end
        else
          redirect_to root_path, alert: 'Document has expired' if @order.expired?
        end
      else
        unless @order.order_items.map(&:document_id).include?(@document.id)
          redirect_to root_path, alert: 'Access Denied'
        end
      end
    end
  end

  def clean_document_price
    params[:document][:price].gsub!('$', '')
  end

  def order_has_document?
    @order.order_items.map(&:document_id).include?(@document.id)
  end
end