class DocumentSafesController < ApplicationController
  before_action :authenticate_user!
  load_and_authorize_resource

  def index
    @document_safes = current_user.document_safes.paginate(
                        page: params[:page], per_page: 30)
  end

  def new
  end

  def show
    respond_to do |format|
      format.json do
        render json: { 
          html: render_to_string(
            template: 'document_safes/show.html.haml', layout: false) }
      end
    end
  end

  def create
    @manager = current_user
    @document_safe = @manager.document_safes.new(document_safe_params)
    if @document_safe.save
      if params[:commit] == 'Create and Make Another Document Safe'
        redirect_to new_document_safe_path(@document_safe = DocumentSafe.new), 
          notice: 'Document safe was successfully created.'
      else
        redirect_to document_safes_path, 
          notice: 'Document safe was successfully created.'
      end
    else
      render 'new', alert: 'Document safe was not created.'
    end
  end

  def destroy
    @document_safe.destroy
    redirect_to document_safes_path, 
      notice: 'Document safe was successfully destroyed.'
  end

  def update
    if @document_safe.update_attributes(document_safe_params)
      redirect_to document_safes_path, 
        notice: 'Document safe was successfully updated.'
    else
      redirect_to document_safes_path, 
        alert: 'Document safe was not updated.'
    end
  end

  private

  def document_safe_params
    params.require(:document_safe).permit(:building_name, :street_address, 
        :city, :state, :zip, :custom_id)
  end
end