class OrdersController < ApplicationController
  include CartOperations
  before_action :authenticate_user!
  before_action :find_document_safe, only: [:new, :create, :order_due_diligence]
  autocomplete :document_safe, :street_address, class_name: 'DocumentSafe'
  load_and_authorize_resource

  def index
    if current_user.manager?
      @document_safes = current_user.document_safes.paginate(
        page: params[:page], per_page: 30)
    else
      @orders = current_user.orders.order(created_at: :desc).paginate(
        page: params[:page], per_page: 30)
    end
  end

  def document_safe_orders
    @orders = DocumentSafe.find(params[:document_safe_id]).orders.order(
      created_at: :desc).paginate(page: params[:page], per_page: 30)
    respond_to do |format|
      format.json do
        render json: { html: render_to_string(
          template: 'orders/document_safe_orders.html.haml', layout: false) }
      end
    end
  end

  def new
    preload_checkout_documents
    @apartment_number = params[:apartment_number]
    @cart_prices = CheckoutCalculator.new(params).cart_prices_info
  end

  def show
    respond_to do |format|
      format.json do
        render json: { html: render_to_string(
          template: 'orders/show.html.haml', layout: false) }
      end
    end
  end

  def create
    @order = current_user.orders.new(
      order_params.merge(document_safe: @document_safe, 
        authorization_code: Order.create_authorization_code))

    if @order.save
      @order.create_order_number
      OrderCheckoutOperations.new(order: @order,
                                  document_ids: create_document_ids,
                                  current_user: current_user).finalize_checkout
      redirect_to root_path, notice: 'Order successful!'
    else
      preload_checkout_documents
      @apartment_number = params[:apartment_number]
      @cart_prices = {price: order_params[:price],
                      sales_tax: order_params[:sales_tax],
                      total_price: order_params[:total_price]}
      render 'new'
    end
  end

  def review_due_diligence
    unless params[:authorization_code].blank?
      @order = Order.find_by(authorization_code: params[:authorization_code])

      if @order
        @offering_plan = @order.offering_plan
        @financial_statements = @order.financial_statements
        @board_minutes = @order.board_minutes
      else
        redirect_to review_due_diligence_path, 
          alert: 'Authorization code is invalid.'
      end
    end
  end
  
  def order_due_diligence
    if @document_safe
      @offering_plans = @document_safe.offering_plans
      @financial_statements = @document_safe.financial_statements.order(
        publish_date: :desc)
      @board_minutes = @document_safe.board_minutes.order(publish_date: :desc)
    end
  end

  def search_document_safes
    @document_safe = DocumentSafe.find_by(street_address: params[:building_address])
    if @document_safe
      redirect_to order_due_diligence_path(document_safe_id: @document_safe.id)
    else
      redirect_to order_due_diligence_path, 
        alert: "#{params[:building_address]} is not in our system."
    end
  end

  def add_to_cart
    if params[:method_caller] == 'order_missing_documents'
      cart_validator = CartValidator.new(
        MissingDocumentFinder.new(params[:order_id]).missing_document_hash)
    else
      cart_validator = CartValidator.new({offeringplan: params[:offeringplan],
                                          financialstatement: params[:financialstatement],
                                          boardminute: params[:boardminute],
                                          document_safe_id: params[:document_safe_id],
                                          method_caller: params[:method_caller]})
    end

    if !params[:order_id].blank? && missing_document_hash_is_empty?
      redirect_to review_due_diligence_path(
        authorization_code: Order.find(params[:order_id]).authorization_code), 
        alert: 'You already have all of the documents for this building'
    elsif cart_validator.valid?
      redirect_to new_order_path(cart_validator.optimize_cart.merge(
        document_safe_id: params[:document_safe_id],
        apartment_number: params[:apartment_number]))
    else
      redirect_to order_due_diligence_path(
        document_safe_id: params[:document_safe_id]), 
        alert: 'There was an error in your cart.'
    end
  end
  
  private

  def order_params
    params.require(:order).permit(:price,
                                  :sales_tax,
                                  :total_price,
                                  :recipient_name,
                                  :recipient_email,
                                  :apartment_number)
  end

  def create_document_ids
    document_ids = []
    document_ids += params[:offeringplan] if params[:offeringplan]
    document_ids += params[:financialstatement] if params[:financialstatement]
    document_ids += params[:boardminute] if params[:boardminute] 
    document_ids
  end

  def find_document_safe
    if params[:document_safe_id]
      @document_safe = DocumentSafe.find(params[:document_safe_id]) 
    end
  end

  def preload_checkout_documents
    unless params[:offeringplan].blank?
      @offering_plans = params[:offeringplan].map { |id| OfferingPlan.find(id) }
    end
    unless params[:freefinancialstatement].blank?
      @free_financial_statements = params[:freefinancialstatement].map { |id| FinancialStatement.find(id) }
      @free_financial_statements = params[:freefinancialstatement].map do |id|
        FinancialStatement.find(id)
      end
    end
    unless params[:financialstatement].blank?
      @financial_statements = params[:financialstatement].map do |id|
        FinancialStatement.find(id)
      end
    end
    unless params[:boardminute].blank?
      @board_minutes = params[:boardminute].map { |id| BoardMinute.find(id) }
    end
  end

  def missing_document_hash_is_empty?
    MissingDocumentFinder.new(
      params[:order_id]).missing_document_hash[:offeringplan].blank? && 
    MissingDocumentFinder.new(
      params[:order_id]).missing_document_hash[:financialstatement].blank? && 
    MissingDocumentFinder.new(
      params[:order_id]).missing_document_hash[:boardminute].blank?
  end
end