class ApplicationController < ActionController::Base
  before_action do
    resource = controller_path.singularize.gsub('/', '_').to_sym
    method = "#{resource}_params"
    params[resource] &&= send(method) if respond_to?(method, true)
  end
  before_action :configure_permitted_parameters, if: :devise_controller?
  protect_from_forgery with: :exception

  rescue_from CanCan::AccessDenied do |exception|
    redirect_to root_url, alert: 'Access Denied'
  end

  def configure_permitted_parameters
    devise_parameter_sanitizer.for(:sign_up) << :type
    devise_parameter_sanitizer.for(:sign_up) << :first_name
    devise_parameter_sanitizer.for(:sign_up) << :last_name
    devise_parameter_sanitizer.for(:sign_up) << :position
    devise_parameter_sanitizer.for(:sign_up) << :phone_number
    devise_parameter_sanitizer.for(:sign_up) << :company_name
    devise_parameter_sanitizer.for(:sign_up) << :company_street_address
    devise_parameter_sanitizer.for(:sign_up) << :company_state
    devise_parameter_sanitizer.for(:sign_up) << :company_zip
    devise_parameter_sanitizer.for(:sign_up) << :customer_uri
    devise_parameter_sanitizer.for(:sign_up) << :credit_card_uri
    devise_parameter_sanitizer.for(:sign_up) << :company_city
    devise_parameter_sanitizer.for(:sign_up) << :bank_account_uri
    devise_parameter_sanitizer.for(:account_update) << :type
    devise_parameter_sanitizer.for(:account_update) << :first_name
    devise_parameter_sanitizer.for(:account_update) << :last_name
    devise_parameter_sanitizer.for(:account_update) << :position
    devise_parameter_sanitizer.for(:account_update) << :phone_number
    devise_parameter_sanitizer.for(:account_update) << :company_name
    devise_parameter_sanitizer.for(:account_update) << :company_street_address
    devise_parameter_sanitizer.for(:account_update) << :company_state
    devise_parameter_sanitizer.for(:account_update) << :company_zip
    devise_parameter_sanitizer.for(:account_update) << :customer_uri
    devise_parameter_sanitizer.for(:account_update) << :credit_card_uri
    devise_parameter_sanitizer.for(:account_update) << :company_city
    devise_parameter_sanitizer.for(:account_update) << :bank_account_uri
  end
end