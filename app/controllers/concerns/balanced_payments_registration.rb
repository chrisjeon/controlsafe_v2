module BalancedPaymentsRegistration
  extend ActiveSupport::Concern

  def lawyer_broker_sign_up(resource)
    credit_card = Balanced::Card.new(credit_card_params)

    if credit_card.save && resource.save
      customer = Balanced::Customer.new(balanced_customer_params).
                 add_card(credit_card.attributes[:uri]).save
      resource.credit_card_uri = credit_card.attributes[:uri]
      resource.customer_uri = customer.attributes[:uri]
      resource.save
      yield resource if block_given?
      if resource.active_for_authentication?
        set_flash_message :notice, :signed_up if is_flashing_format?
        sign_up(resource_name, resource)
        respond_with resource, :location => after_sign_up_path_for(resource)
      else
        set_flash_message :notice, :"signed_up_but_#{resource.inactive_message}" if is_flashing_format?
        expire_data_after_sign_in!
        respond_with resource, :location => after_inactive_sign_up_path_for(resource)
      end
    else
      clean_up_passwords resource
      respond_with resource
    end
  end

  def manager_sign_up(resource)
    bank_account = Balanced::BankAccount.new(bank_account_params.merge(type: 'checking'))
    if bank_account.save && resource.save
      customer = Balanced::Customer.new(balanced_customer_params).
                 add_bank_account(bank_account.attributes[:uri]).save
      resource.bank_account_uri = bank_account.attributes[:uri]
      resource.customer_uri = customer.attributes[:uri]
      resource.save
      yield resource if block_given?
      if resource.active_for_authentication?
        set_flash_message :notice, :signed_up if is_flashing_format?
        sign_up(resource_name, resource)
        respond_with resource, :location => after_sign_up_path_for(resource)
      else
        set_flash_message :notice, :"signed_up_but_#{resource.inactive_message}" if is_flashing_format?
        expire_data_after_sign_in!
        respond_with resource, :location => after_inactive_sign_up_path_for(resource)
      end
    else
      clean_up_passwords resource
      respond_with resource
    end
  end
end