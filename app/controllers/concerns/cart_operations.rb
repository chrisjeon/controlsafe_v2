module CartOperations
  extend ActiveSupport::Concern

  class CartValidator
    attr_reader :documents, :offering_plans, :financial_statements, 
                :board_minutes, :document_safe, :method_caller

    def initialize(documents={})
      @document_safe = DocumentSafe.find(documents[:document_safe_id])
      @documents = documents
      @offering_plans = []
      @financial_statements = []
      @free_financial_statements = []
      @board_minutes = []
      @method_caller = documents[:method_caller]
      add_documents_to_arrays
    end

    def valid?
      return true if method_caller == 'order_missing_documents'
      return false if empty_shopping_cart?
      if board_minutes_valid? && financial_statements_valid? && offering_plan_valid?
        return true
      end
    end

    def optimize_cart
      create_free_financial_statements unless offering_plans.empty?
      { offeringplan: @offering_plans,
        freefinancialstatement: @free_financial_statements,
        financialstatement: @financial_statements,
        boardminute: @board_minutes}
    end

    private

    def create_free_financial_statements
      if @financial_statements.empty?
        if document_safe.financial_statements.count >= 2
          @free_financial_statements = document_safe.financial_statements.
                                       order(publish_date: :desc).map(&:id).
                                       first(2)
        elsif document_safe.financial_statements.count == 1
          @free_financial_statements = 
            document_safe.financial_statements.map(&:id)
        end
      else
        2.times { @free_financial_statements << @financial_statements.pop }
        @free_financial_statements.reverse!
      end
    end

    def empty_shopping_cart?
      true if board_minutes.empty? && financial_statements.empty? && offering_plans.empty?
    end

    def offering_plan_valid?
      true
    end

    def board_minutes_valid?
      if board_minutes.empty?
        return true
      else
        return true if board_minutes.size >= 24
      end
    end

    def financial_statements_valid?
      if financial_statements.empty?
        return true
      else
        return true if financial_statements.size >= 2
      end
    end

    def add_documents_to_arrays
      if documents[:offeringplan]
        documents[:offeringplan].each do |offering_plan_id|
          @offering_plans << offering_plan_id
        end
      end
      if documents[:financialstatement]
        documents[:financialstatement].each do |financial_statement_id|
          @financial_statements << financial_statement_id
        end
      end
      if documents[:boardminute]
        documents[:boardminute].each do |board_minute_id|
          @board_minutes << board_minute_id
        end
      end
    end
  end

  class CheckoutCalculator
    attr_reader :documents, :offering_plans, :financial_statements, 
                :board_minutes

    def initialize(documents={})
      @documents = documents
      @offering_plans = []
      @financial_statements = []
      @free_financial_statements = []
      @board_minutes = []
      @price = 0.00
      @sales_tax = 0.00
      @total_price = 0.00
      add_documents_to_arrays
    end

    def cart_prices_info
      calculate_total_price
      { price: clean_price(@price.to_s),
        sales_tax: clean_price(@sales_tax.to_s),
        total_price: clean_price(@total_price.to_s) }
    end
    
    private

    def clean_price(price)
      price[-1] == '0' && price[-2] == '.' ? price + '0' : price
    end

    def calculate_total_price
      calculate_price
      calculate_sales_tax
      @total_price = @price + @sales_tax
    end

    def calculate_price
      (@offering_plans + @financial_statements + @board_minutes).each do |document|
        @price += document.price.to_f
      end
    end

    def calculate_sales_tax
      @sales_tax = (@price * 0.08875).round(2)
    end

    def add_documents_to_arrays
      if documents[:offeringplan]
        documents[:offeringplan].each do |offering_plan_id|
          @offering_plans << OfferingPlan.find(offering_plan_id)
        end
      end
      if documents[:freefinancialstatement]
        documents[:freefinancialstatement].each do |offering_plan_id|
          @free_financial_statements << FinancialStatement.find(offering_plan_id)
        end
      end
      if documents[:financialstatement]
        documents[:financialstatement].each do |financial_statement_id|
          @financial_statements << FinancialStatement.find(financial_statement_id)
        end
      end
      if documents[:boardminute]
        documents[:boardminute].each do |board_minute_id|
          @board_minutes << BoardMinute.find(board_minute_id)
        end
      end
    end
  end

  class OrderCheckoutOperations
    attr_reader :order, :document_ids, :current_user

    def initialize(order_information)
      @order = order_information[:order]
      @document_ids = order_information[:document_ids]
      @current_user = order_information[:current_user]
    end

    def finalize_checkout
      send_emails
      create_order_items
      charge_current_user
    end

    private

    def send_emails
      UserMailer.order_confirmation_buyer_email(
          current_user.email, order.order_number).deliver
      UserMailer.order_confirmation_lawyer_email(
          @order.recipient_email, @order.authorization_code).deliver
    end

    def create_order_items
      document_ids.each do |document_id|
        order.order_items.create(document_id: document_id)
      end
    end

    def charge_current_user
      Balanced::Customer.find(current_user.customer_uri).debit(
        :amount => (order.total_price.to_f.round(2) * 100).to_i)
    end
  end

  class MissingDocumentFinder
    attr_reader :order, :document_safe

    def initialize(order_id)
      @order = Order.find(order_id)
      @document_safe = @order.document_safe
    end

    def missing_document_hash
      { offeringplan: missing_offering_plan_ids,
        financialstatement: missing_financial_statement_ids,
        boardminute: missing_board_minute_ids,
        document_safe_id: document_safe.id,
        apartment_number: order.apartment_number,
        method_caller: 'order_missing_documents' }
    end

    private
    
    def missing_document_ids
      @document_safe.documents.map(&:id) - @order.order_items.map(&:document_id)
    end

    def missing_offering_plan_ids
      missing_document_ids.map do |id|
        id if Document.find(id).offering_plan?
      end.reject { |id| id.nil? }
    end

    def missing_financial_statement_ids
      missing_document_ids.map do |id|
        id if Document.find(id).financial_statement?
      end.reject { |id| id.nil? }
    end

    def missing_board_minute_ids
      missing_document_ids.map do |id|
        id if Document.find(id).board_minute?
      end.reject { |id| id.nil? }
    end
  end
end
