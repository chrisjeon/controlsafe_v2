module BalancedPaymentsUpdate
  extend ActiveSupport::Concern

  def lawyer_broker_update(resource, prev_unconfirmed_email)
    Balanced::Card.find(resource.credit_card_uri).unstore
    credit_card = Balanced::Card.new(credit_card_params)
    if credit_card.save && update_resource(resource, account_update_params)
      yield resource if block_given?
      customer = Balanced::Customer.find(resource.customer_uri).
                 add_card(credit_card.attributes[:uri]).save
      resource.credit_card_uri = credit_card.attributes[:uri]
      resource.customer_uri = customer.attributes[:uri]
      resource.save
      if is_flashing_format?
        flash_key = update_needs_confirmation?(resource, prev_unconfirmed_email) ?
        :update_needs_confirmation : :updated
        set_flash_message :notice, flash_key
      end
      sign_in resource_name, resource, :bypass => true
      respond_with resource, :location => after_update_path_for(resource)
    else
      clean_up_passwords resource
      respond_with resource
    end
  end

  def manager_update(resource, prev_unconfirmed_email)
    Balanced::BankAccount.find(resource.bank_account_uri).unstore
    bank_account = Balanced::BankAccount.new(bank_account_params.merge(type: 'checking'))
    if bank_account.save && update_resource(resource, account_update_params)
      yield resource if block_given?
      customer = Balanced::Customer.find(resource.customer_uri).
                 add_bank_account(bank_account.attributes[:uri]).save
      resource.bank_account_uri = bank_account.attributes[:uri]
      resource.customer_uri = customer.attributes[:uri]
      resource.save
      if is_flashing_format?
        flash_key = update_needs_confirmation?(resource, prev_unconfirmed_email) ?
        :update_needs_confirmation : :updated
        set_flash_message :notice, flash_key
      end
      sign_in resource_name, resource, :bypass => true
      respond_with resource, :location => after_update_path_for(resource)
    else
      clean_up_passwords resource
      respond_with resource
    end
  end
end