class RegistrationsController < Devise::RegistrationsController
  include BalancedPaymentsRegistration
  include BalancedPaymentsUpdate
  def create
    build_resource(sign_up_params)

    if sign_up_params[:type] == 'Manager'
      bank_account_params[:account_number].blank? ? super : manager_sign_up(resource)
    else
      credit_card_params[:card_number].blank? ? super : lawyer_broker_sign_up(resource)
    end
  end

  def update
    self.resource = resource_class.to_adapter.get!(send(:"current_#{resource_name}").to_key)
    if resource.respond_to?(:unconfirmed_email)
      prev_unconfirmed_email = resource.unconfirmed_email
    end
    if resource.type == 'Manager'
      if bank_account_params[:account_number].blank?
        super
      else
        manager_update(resource, prev_unconfirmed_email)
      end
    else
      if credit_card_params[:card_number].blank?
        super
      else
        lawyer_broker_update(resource, prev_unconfirmed_email)
      end
    end
  end

  private

  def credit_card_params
    params.require(:credit_card).permit(
      :card_number, :expiration_year, :expiration_month, :security_code, 
      :name, :phone_number, :city, :state, :postal_code, :street_address)
  end

  def bank_account_params
    params.require(:bank_account).permit(:name, :routing_number, :account_number)
  end

  def balanced_customer_params
    {name: "#{sign_up_params[:first_name]} #{sign_up_params[:last_name]}", 
     email: sign_up_params[:email]}
  end
end