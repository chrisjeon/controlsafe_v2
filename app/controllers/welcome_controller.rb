class WelcomeController < ApplicationController
  def index
  end

  def terms_and_conditions
    @template_directory = ''
    if params[:user_type] == 'manager'
      @template_directory = 'welcome/terms_and_conditions/manager_terms'
    elsif params[:user_type] == 'broker'
      @template_directory = 'welcome/terms_and_conditions/broker_terms'
    elsif params[:user_type] == 'lawyer'
      @template_directory = 'welcome/terms_and_conditions/lawyer_terms'
    end
  end

  def swap_forms
    respond_to do |format|
      if params[:user_type] == 'manager'
        format.json do
          render json: { 
            html: render_to_string(
              template: 'welcome/_manager_registration_form.html.haml', 
              layout: false) }
        end
      else
        format.json do
          render json: { 
            html: render_to_string(
              template: 'welcome/_lawyer_broker_registration_form.html.haml', 
              layout: false) }
        end
      end
    end
  end

  def send_email
    UserMailer.contact_form_email(
      params[:name], params[:email], params[:subject], params[:content]).deliver
    redirect_to root_path, notice: 'Your inquiry was sent'
  end
end