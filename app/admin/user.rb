ActiveAdmin.register User do
  permit_params :type, :email, :password, :password_confirmation, :first_name, 
                :last_name, :position, :phone_number, :company_name, 
                :company_street_address, :company_state, :company_city, 
                :company_zip 

  form do |f|
    f.inputs "Details" do
      f.input :type, as: :select, collection: User.all.map(&:type).uniq
      f.input :email
      f.input :first_name
      f.input :last_name
      f.input :position
      f.input :phone_number
      f.input :company_name
      f.input :company_street_address
      f.input :company_state
      f.input :company_city
      f.input :company_zip
    end
    f.actions
  end

  index do
    id_column
    column :type
    column :email
    column :first_name
    column :last_name
    column :position
    column :phone_number
    column :company_name
    column :company_street_address
    column :company_state
    column :company_city
    column :company_zip
    column :created_at
    column :updated_at
    default_actions
  end

  filter :type, as: :select, collection: proc { User.all.map(&:type) }
  filter :email
  filter :first_name
  filter :last_name
  filter :position
  filter :phone_number
  filter :company_name
  filter :company_street_address
  filter :company_state
  filter :company_city
  filter :company_zip
  filter :created_at
  filter :updated_at    
end
