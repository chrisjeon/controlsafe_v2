ActiveAdmin.register Document do
  permit_params :type, :price, :publish_date, :doc_file_name, :doc_updated_at
  
  form do |f|
    f.inputs "Details" do
      f.input :type, as: :select, collection: Document.all.map(&:type).uniq
      f.input :price
      f.input :publish_date
      f.input :doc, as: :file
      f.input :doc_file_name
    end
    f.actions
  end

  index do
    id_column
    column :type
    column :price
    column :publish_date
    column :doc do |document|
      link_to document.doc, document.doc.expiring_url(3600) 
    end
    column :doc_file_name
    column :doc_content_type
    column :doc_file_size
    column :doc_updated_at
    column :created_at
    column :updated_at
    default_actions
  end

  filter :type, as: :select, collection: proc { Document.all.map(&:type).uniq }
  filter :document_safe_building_name, as: :select, 
    collection: proc { DocumentSafe.all.map(&:building_name) }, label: 'Building Name'
  filter :title
  filter :price
  filter :doc_file_name
  filter :publish_date
  filter :created_at
  filter :updated_at
end