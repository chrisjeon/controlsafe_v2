ActiveAdmin.register Order do
  permit_params :price, :sales_tax, :total_price, :recipient_name, 
                :recipient_email, :authorization_code, :apartment_number
  
  filter :document_safe_building_name, as: :select, 
          collection: proc { DocumentSafe.all.map(&:building_name) },
          label: 'Building Name'
  filter :price
  filter :sales_tax
  filter :total_price
  filter :recipient_name
  filter :recipient_email
  filter :authorization_code
  filter :apartment_number
  filter :created_at
  filter :updated_at

  form do |f|
    f.inputs "Details" do
      f.input :price
      f.input :sales_tax
      f.input :total_price
      f.input :recipient_name
      f.input :recipient_email
      f.input :authorization_code
      f.input :apartment_number
    end
    f.actions
  end
end
