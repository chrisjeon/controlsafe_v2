ActiveAdmin.register DocumentSafe do
  permit_params :building_name, :street_address, :city, :state, :zip, 
                :custom_id, :manager_id, :created_at, :updated_at
  
  filter :orders, as: :select, 
    collection: proc { Order.all.map(&:order_number) }, label: 'Order #'
  filter :manager_first_name, as: :select, 
    collection: proc { Manager.all.map(&:first_name) }
  filter :manager_last_name, as: :select, 
    collection: proc { Manager.all.map(&:last_name) }
  filter :building_name
  filter :street_address
  filter :city
  filter :state
  filter :zip
  filter :created_at
  filter :updated_at

  form do |f|
    f.inputs "Details" do
      f.input :building_name
      f.input :street_address
      f.input :city
      f.input :state
      f.input :zip
    end
    f.actions
  end  
end
